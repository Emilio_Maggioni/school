package it.vidiemme.school.webapi.cucumber.steps;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.web.WebAppConfiguration;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import it.vidiemme.school.webapi.config.TestAppConfig;
import it.vidiemme.school.webapi.config.CucumberConfig;
import it.vidiemme.school.webapi.config.TestRepositoryConfig;
import it.vidiemme.school.webapi.config.WebConfig;
import it.vidiemme.school.webapi.cucumber.worlds.EndToEndTestsWorld;
import it.vidiemme.school.webapi.web.controller.GenericController;

@WebAppConfiguration
@TestPropertySource(locations = {
		"classpath:properties/app.properties", 
		"classpath:properties/mail.properties", 
		"classpath:properties/jdbc.properties"
		})
@ContextConfiguration(classes = {CucumberConfig.class, TestAppConfig.class, TestRepositoryConfig.class, WebConfig.class})
public class EndToEndTestSteps {

	@Autowired
	private EndToEndTestsWorld world;
	
	@Autowired
	private GenericController apiController;
	
	@Given("^A running environment$")
    public void checkEnvironment() {
        assertNotNull(apiController);
    }
	
	@When("^Calling the version API$")
    public void callVersionController() {
        world.version = apiController.getVersion().get("api-version");
    }
	
	@Then("^Verify that the version API correctly responds$") 
	public void verifyVersionControllerResponse() {
		assertEquals(world.version, "v1");
	}
	
}
