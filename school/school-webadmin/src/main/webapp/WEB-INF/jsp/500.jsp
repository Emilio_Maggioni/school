<%@ include file="/includes/taglibs.jsp"%>
<!DOCTYPE html>
<html lang="en">
	<head>
	    <title>500 <spring:message code="project.brand"/></title>
	</head>
	<body>
	    <div class="middle-box text-center animated fadeInDown">
	        <h1>500</h1>
	        <h3 class="font-bold"><spring:message code="error.title"/></h3>
	        <div class="error-desc"></div>
	    </div>
	</body>
</html>
