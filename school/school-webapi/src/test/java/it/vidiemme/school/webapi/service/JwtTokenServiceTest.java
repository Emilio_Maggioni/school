package it.vidiemme.school.webapi.service;

import static org.junit.Assert.assertNotEquals;
import static org.junit.Assert.assertNotNull;
import static org.junit.Assert.assertTrue;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import it.vidiemme.school.business.model.User;
import it.vidiemme.school.business.security.UserPrincipal;
import it.vidiemme.school.webapi.config.JUnitTestBase;
import it.vidiemme.school.webapi.web.dto.JwtAuthenticationResponse;

/**
 * Tests JwtTokenService main features like JWT generation given an existing username and password
 * Since is not necessary to test database connectivity, authentication manager and user details service
 * behaviors are mocked using Mockito.
 * 
 * Mocked services are annotated with @Mock; the services that we are testing that will use these mocks is annotated
 * with @InjectMock; it is mandatory to call MockitoAnnotations.initMocks(this) to initialize the mocks before each test
 * 
 * @author flavio.ricci
 */
@DisplayName("JWT related features")
@Tag("JWT")
@ExtendWith(SpringExtension.class)
public class JwtTokenServiceTest extends JUnitTestBase {

	// SUT (system under test): injecting mockito mocks defined below
	@Autowired
	@InjectMocks
	private JwtTokenService jwtTokenService;
	
	@Mock
	private UserDetailsService userDetailsServiceMock;
	
	@Mock
	private AuthenticationManager authenticationManagerMock;
	
	/**
	 * Create a fake user and mock user details service in order to return the mocked user
	 */
	@BeforeEach
	public void setUp() {
		//init mocks defined with @Mock annotation
		MockitoAnnotations.initMocks(this);
		
		//building a test user
		UserPrincipal userPrincipal = buildTestUser();
        
        //defining mocks behavior
		Mockito.when(authenticationManagerMock.authenticate(Mockito.any())).thenReturn(null);
		Mockito.when(userDetailsServiceMock.loadUserByUsername("testuser")).thenReturn(userPrincipal);
	}

	@Test
    public void whenLoggingIn_thenReturnedJWTIsCorrect() {
		//given
		String username = "testuser";
		String password = "admin";
		
		//when
		JwtAuthenticationResponse resp = jwtTokenService.login(username, password);
		String jwt = resp.getToken();
		
		//then
		String jwtUsername = jwtTokenService.getUserDetails(jwt).getUsername();
		assertNotNull(jwtUsername);
		assertTrue(jwtUsername.equals(username));
    }
	
	@Test
    public void whenRefreshingToken_thenTokenIsDifferent() {
		//given
		String username = "testuser";
		String password = "admin";
		
		//when
		JwtAuthenticationResponse resp = jwtTokenService.login(username, password);
		String jwt = resp.getToken();
		
		//then
		String refreshedJWT = jwtTokenService.refreshToken(jwt);
		assertNotEquals(jwt, refreshedJWT);
    }
	
	/**
	 * Builds a fake user used for tests
	 * @return
	 */
	private UserPrincipal buildTestUser() {
		List<String> privileges = new ArrayList<>();
		privileges.add("TESTPRIVILEGE");
		
		User user = new User();
		user.setUsername("testuser");
		user.setPrivileges(privileges);
		user.setIsEnabled(Boolean.TRUE);
		
		Set<GrantedAuthority> grantedAuthorites = new HashSet<>();
        user.getPrivileges().stream().map(p -> new SimpleGrantedAuthority(p)).forEach(grantedAuthorites::add);        
        
        UserPrincipal userPrincipal = new UserPrincipal(user, grantedAuthorites);
		return userPrincipal;
	}
	
}
