<%@ include file="/includes/taglibs.jsp"%>
<!DOCTYPE html>
<html lang="en">
	<head>
	    <title>404 <spring:message code="project.brand"/></title>
	</head>
	<body>
	    <div class="middle-box text-center animated fadeInDown">
	        <h1>404</h1>
	        <h3 class="font-bold"><spring:message code="notFound.title"/></h3>
	        <div class="error-desc"><spring:message code="notFound.text"/></div>
	    </div>
	</body>
</html>
