<%@include file="/includes/taglibs.jsp"%>
<form:errors path="" cssClass="alert alert-danger" element="div" cssStyle="margin-bottom:20px" />
<form:hidden path="id"/>
<div class="row">
    <div class="col-md-3 col-lg-3">
        <spring:bind path="username">
            <div class="form-group  ${status.error ? 'has-error' : ''}">
                <form:label path="username" class="control-label">
                    <spring:message code="user.username" /> *
                </form:label>
                <form:input path="username" cssClass=" form-control" cssErrorClass="form-control is-invalid"  required="required"/>
                <form:errors path="username" cssClass="invalid-feedback" element="span" />
            </div>
        </spring:bind>
    </div>
    <div class="col-md-3 col-lg-3">
        <spring:bind path="firstName">
            <div class="form-group ${status.error ? 'has-error' : ''}" >

                <form:label path="firstName" class="control-label">
                    <spring:message code="user.firstName" /> *
                </form:label>
                <form:input path="firstName" cssClass=" form-control" cssErrorClass="form-control is-invalid" required="required"/>
                <form:errors path="firstName" cssClass="invalid-feedback" element="span" />
            </div>
        </spring:bind>
    </div>
    <div class="col-md-3 col-lg-3">
        <spring:bind path="lastName">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <form:label path="lastName" class="control-label">
                    <spring:message code="user.lastName" /> *
                </form:label>
                <form:input path="lastName" cssClass=" form-control" cssErrorClass=" form-control is-invalid" required="required"/>
                <form:errors path="lastName" cssClass="invalid-feedback" element="span" />
            </div>
        </spring:bind>
    </div>
    <div class="col-md-3 col-lg-3">
        <spring:bind path="roles">
            <div class="form-group ${status.error ? 'has-error' : ''}">
                <label for="roles"  class="control-label" >
                    <spring:message code="user.roles" /> *
                </label>
                <form:select path="roles" class="chosen-select form-control" multiple="multiple" data-placeholder="---" cssErrorClass="chosen-select is-invalid form-control">
                    <option label="---" value="" />
                    <c:forEach items="${roleList}" var="role">
                        <c:set value="" var="selected"></c:set>
                        <c:forEach items="${userForm.roles}" var="userRole">
                            <c:if test="${role.id == userRole}">
                                <c:set value="selected" var="selected"></c:set>
                            </c:if>
                        </c:forEach>
                        <option value="${role.id}" ${selected}><spring:message code="${role.name}" /></option>
                    </c:forEach>
                </form:select>
                <form:errors path="roles" cssClass="invalid-feedback" element="span" />
            </div>
        </spring:bind>
    </div>
</div>
<div class="row">
    <div class="form-group">
    	<div class="col-sm-4">
        	<button class="btn btn-primary" type="submit"><spring:message code="save" /></button>
    	</div>
    </div>
</div>

