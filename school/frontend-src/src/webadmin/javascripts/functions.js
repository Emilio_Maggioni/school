/**
 * Funzione per la gestione automatica delle confirm
 *
 * @param msg
 * @param textok
 * @param textko
 * @param form
 * @param okcb
 */
function openConfirmDelete(msg, textok, textko, form, okcb) {
    bootbox.confirm({
        size: "small",
        message: msg,
        className: 'inmodal',
        callback: function (result) {
            if (result) {
                if (form) {
                    $(form).submit();
                }
                else {
                    okcb(result);
                }
            }
        }
    });
}

/**
 * Funzione per mostrare un errore in un alert
 *
 * @param error
 * @param cb
 */
function displayNetworkError(error, cb) {
    bootbox.alert({
        size: 'small',
        message: error || i18next.t('errors.networkError'),
        className: 'inmodal',
        callback: cb,
    });
}


/**
 * Funzione che serializza il form di ricerca per DataTable
 *
 * @param formId
 * @returns
 */
function jsonFilter(formId) {
    return {
        filter: $('#' + formId).serializeObject(),
    };
}
