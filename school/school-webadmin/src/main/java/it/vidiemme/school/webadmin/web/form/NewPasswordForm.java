package it.vidiemme.school.webadmin.web.form;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

public class NewPasswordForm {

    @NotEmpty
	private String token;
    @Pattern(regexp="((?=.*[0-9])(?=.*[a-zA-Z]).{8,20})")
	private String newPassword;
	private String confirmNewPassword;

	public String getToken() {
		return token;
	}

	public void setToken(String token) {
		this.token = token;
	}

	public String getNewPassword() {
		return newPassword;
	}

	public void setNewPassword(String newPassword) {
		this.newPassword = newPassword;
	}

	public String getConfirmNewPassword() {
		return confirmNewPassword;
	}

	public void setConfirmNewPassword(String confirmNewPassword) {
		this.confirmNewPassword = confirmNewPassword;
	}
}
