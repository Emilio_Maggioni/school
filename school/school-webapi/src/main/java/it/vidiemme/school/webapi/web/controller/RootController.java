package it.vidiemme.school.webapi.web.controller;

import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Root controller with no API base path
 */
@RestController
public class RootController {

	private static final Logger logger = LoggerFactory.getLogger(RootController.class);
	
    /**
     * The purpose of this API is to respond to resource monitor periodic calls without returning an error 
     */
    @GetMapping(value="", produces = MediaType.APPLICATION_JSON)
    public ResponseEntity<?> checkResourceStatus() {
    	logger.debug("Calling root controller");
        return ResponseEntity.status(HttpStatus.OK).build();
    }
}