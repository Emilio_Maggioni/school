import { connect } from 'react-redux';

import component from './component';

import authenticationAction from '../../actions/authentication';
import AuthenticationService from '../../services/authentication';

const authenticationService = new AuthenticationService();

/**
 * Mappa lo stato sulle proprietà del componente
 *
 * @ignore
 * @param {Object} state
 */
const mapStateToProps = state => ({
    loading: state.authentication.loading,
    authenticated: state.authentication.authenticated,
});

/**
 * Mappa gli handler del componente sulle action
 *
 * @ignore
 * @param {function} dispatch
 */
const mapDispatchToProps = dispatch => ({
    /**
     * Gestisce il submit del form di login
     *
     * @param {string} username Username dell'utente
     * @param {string} password Password dell'utente
     */
    handleLogin(username, password) {
        if (username.length > 0 && password.length > 0) {
            dispatch(authenticationAction.signin({
                status: 'loading',
            }));
            // @TODO: show error in catch function
            authenticationService.signin(username, password).then((data) => {
                dispatch(authenticationAction.signin(data));
            }).catch((error) => {
                dispatch(authenticationAction.signin(error.data));
            });
        }
    },
});

const Login = connect(
    mapStateToProps,
    mapDispatchToProps,
)(component);

export default Login;
