/*
 * Authentication action types
 */
export const SIGNIN = 'SIGNIN';
export const SIGNOUT = 'SIGNOUT';


/**
 * Authentication action creators
 *
 * @module authentication
 */
const authentication = {
    /**
     * Action per gestire la login
     *
     * @param {Object} data
     * @returns {Object} Action
     */
    signin(data) {
        return {
            type: SIGNIN,
            status: data.status,
            data,
        };
    },

    /**
     * Action per gestire la logout
     *
     * @param {Object} data
     * @returns {Object} Action
     */
    signout(data) {
        return {
            type: SIGNOUT,
            status: data.status,
            data,
        };
    },
};

export default authentication;
