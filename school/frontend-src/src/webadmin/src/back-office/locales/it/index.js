import commons from './commons';
import login from './login';

const it = {
    commons,
    login,
};

export default it;
