<%@ include file="/includes/taglibs.jsp"%>
<!DOCTYPE html>
<html lang="${pageContext.response.locale}">
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
		<meta charset="utf-8" />
		<meta http-equiv="X-UA-Compatible" content="IE=edge" />
        <meta name="viewport" content="width=device-width,initial-scale=1,maximum-scale=1,minimum-scale=1,user-scalable=no" />
		<meta name="description" content="" />
		<meta name="author" content="" />
        <meta name="_csrf" content="${_csrf.token}"/>
        <meta name="_csrf_header" content="${_csrf.headerName}"/>
        <link rel="shortcut icon" href="${ctx}/assets/back-office/favicon.ico">
		<title><sitemesh:write property='title'/></title>

        <link href="${ctx}/assets/back-office/back-office.css" rel="stylesheet">

		<sitemesh:write property='head'/>
	</head>
	<body>
		<sitemesh:write property='body'/>
        <script type="text/javascript" src="${ctx}/assets/back-office/back-office-index.js"></script>
        <script type="text/javascript" src="${ctx}/assets/back-office/back-office.js"></script>
        <script type="text/javascript" src="${ctx}/assets/back-office/back-office-config.js"></script>
		<sitemesh:write property="page.localscript"></sitemesh:write>
	</body>
</html>
