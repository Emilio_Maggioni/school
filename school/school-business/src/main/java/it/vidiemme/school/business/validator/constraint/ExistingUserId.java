package it.vidiemme.school.business.validator.constraint;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import javax.validation.Constraint;
import javax.validation.Payload;

import org.springframework.stereotype.Component;

@Component
@Constraint(validatedBy = ExistingUserIdConstraint.class)
@Retention(RetentionPolicy.RUNTIME)
@Target({ ElementType.PARAMETER, ElementType.FIELD })
public @interface ExistingUserId {

    public String message() default "The user was not found";

    public Class<?>[] groups() default {};

    public Class<? extends Payload>[] payload() default {};
}
