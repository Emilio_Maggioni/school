INSERT INTO `roles` (`id`, `name`) VALUES
    (1, 'ROLE_VIEWER'),
    (2, 'ROLE_ADMIN');

INSERT INTO `roles_privileges` (`role_id`, `privilege_name`) VALUES
    (1, 'EXAMPLE_SECTION_VIEW'),
    (2, 'EXAMPLE_SECTION_VIEW'),
    (2, 'USERS_MANAGE');

INSERT INTO `users` (`id`, `username`, `password`, `first_name`, `last_name`, `is_enabled`, `created_at`, `updated_at`, `last_access`, `temp_password`, `is_deleted`) VALUES
    (1, 'test_viewer@vidiemme.it', '$2a$10$A661HCsqALAe1Oxps9YG2O2Pco23CziJ7P/Q1gNltwnzVHysmcgpy', 'Viewer', 'User', 1, '2018-03-14 12:09:30', '2018-03-14 12:09:31', '2018-03-14 12:09:31', 0, 0),
    (2, 'test_admin@vidiemme.it', '$2a$10$n8KkGnjFZALVkIOQMa81F.DC0MelJXANMp4PHVDmEluym0lQRsEke', 'Admin', 'User', 1, '2018-03-16 10:53:25', '2018-03-16 10:53:25', '2018-03-16 10:53:26', 0, 0);

INSERT INTO `users_roles` (`user_id`, `role_id`) VALUES
    (1, 1),
    (2, 2);
    
COMMIT;
