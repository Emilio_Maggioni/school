package it.vidiemme.school.webadmin.web.controller;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.NoHandlerFoundException;
import org.springframework.web.servlet.mvc.support.DefaultHandlerExceptionResolver;

@ControllerAdvice
public class ControllerExceptionHandler extends DefaultHandlerExceptionResolver {
    private static final Logger LOG = LoggerFactory.getLogger(ControllerExceptionHandler.class);

    @ExceptionHandler(Exception.class)
    public String handleGenericException(HttpServletRequest request, Exception e) {
        if (e instanceof AccessDeniedException) {
            return "403";
        } else if (e instanceof NoHandlerFoundException) {
            return "404";
        } else {
            LOG.error(null, e);
            return "500";
        }
    }
}