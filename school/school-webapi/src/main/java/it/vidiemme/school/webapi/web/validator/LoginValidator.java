package it.vidiemme.school.webapi.web.validator;

import javax.validation.groups.Default;

import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.SmartValidator;

import it.vidiemme.school.webapi.web.dto.JwtAuthenticationRequest;

/**
 * Validator injected in login controller
 */
@Component
public class LoginValidator implements SmartValidator {

	@Override
	public boolean supports(Class<?> clazz) {
		return JwtAuthenticationRequest.class.equals(clazz);
	}
	
    @Override
    public void validate(Object target, Errors errors, Object... validationHintsArray) {
        applyValidation(target, errors, validationHintsArray);
    }

    @Override
    public void validate(Object target, Errors errors) {
        applyValidation(target, errors, Default.class);
    }	

    public void applyValidation(Object target, Errors errors, Object... validationHintsArray) {
        JwtAuthenticationRequest jwtAuthenticationRequest = (JwtAuthenticationRequest) target;
    }
}
