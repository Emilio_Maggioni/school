package it.vidiemme.school.business.security;

import java.util.Collection;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import it.vidiemme.school.business.model.User;

public class UserPrincipal implements UserDetails {
    private static final long serialVersionUID = 1L;

    private User user;
    private Collection<GrantedAuthority> grantedAuthorites;

    public UserPrincipal(User user, Collection<GrantedAuthority> grantedAuthorites) {
        super();
        this.user = user;
        this.grantedAuthorites = grantedAuthorites;
    }

    public User getUser() {
        return user;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return user.getIsEnabled();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return grantedAuthorites;
    }

    @Override
    public String getPassword() {
        return user.getPassword();
    }

    @Override
    public String getUsername() {
        return user.getUsername();
    }
}
