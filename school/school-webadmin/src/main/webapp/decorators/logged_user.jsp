<%@ include file="/includes/taglibs.jsp"%>
<security:authentication property="principal" var="userPrincipal" />
<head>
    <sitemesh:write property='head' />
    <title><sitemesh:write property='title'/></title>
</head>
<body>
	<div id="wrapper">

	    <!-- Side menu -->
		<nav class="navbar-default navbar-static-side" role="navigation">
			<div class="sidebar-collapse">
				<ul class="nav metismenu" id="side-menu">
					<li class="nav-header">
                        <div class="profile-element">
                            <i class="icon fa fa-user-circle"></i>
                            <span class="block user-name m-t"><strong class="font-bold">${userPrincipal.user.firstName} ${userPrincipal.user.lastName}</strong></span>
                        </div>
                        <!-- <div class="logo-element">
                            <i class="icon fa fa-user-circle"></i>
                        </div> -->
					</li>
					<sitemesh:write property='page.mainmenu' />
				</ul>
			</div>
		</nav>

		<main id="page-wrapper" class="gray-bg">

	        <header class="row border-bottom">
	            <nav class="navbar navbar-static-top" role="navigation" style="margin-bottom: 0">
	                <div class="navbar-header">
	                    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#"><i class="fa fa-bars"></i> </a>
	                </div>
	                <ul class="nav navbar-top-links navbar-right">
	                    <li>
	                        <form action="${ctx}/logout" method="POST" class="form-inline require-confirm" data-message="Uscire dall'applicazione?">
	                            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
	                            <button type="submit" class="btn btn-link">
	                                <i class="fa fa-sign-out"></i> Log out
	                            </button>
	                        </form>
	                    </li>
	                </ul>
	            </nav>
            </header>

			<!-- MAIN CONTENT -->
			<sitemesh:write property='body' />

			<!-- STICKY FOOTER -->
			<footer class="row">
				<div class="footer">
					<div>
						<strong>Copyright</strong>&nbsp; Vidiemme &copy; 2019
					</div>
				</div>
			</footer>
		</main>
	</div>
</body>

<content tag="localscript"> <sitemesh:write property="page.localscript"></sitemesh:write> </content>

</html>
