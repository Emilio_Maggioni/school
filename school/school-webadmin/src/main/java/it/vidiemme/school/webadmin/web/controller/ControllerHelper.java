package it.vidiemme.school.webadmin.web.controller;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.stereotype.Component;

@Component
public class ControllerHelper {
    
    /** Determines the default page to show to a logged user, basing on the application sections he has the permission to view.
     * @param authentication the object representing the token for an authentication
     * @return the user default url
     */
    public String getUserDefaultUrl(Authentication authentication) {
        
        String targetUrl = null;
        Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
        List<String> privileges = authorities.stream().map(GrantedAuthority::getAuthority).collect(Collectors.toList());

        if (privileges.contains("USERS_MANAGE"))
            targetUrl = "/user";
        else if (privileges.contains("EXAMPLE_SECTION_VIEW"))
            targetUrl = "/example-section";

        if (targetUrl == null)
            throw new IllegalStateException();

        return targetUrl;
    }
}
