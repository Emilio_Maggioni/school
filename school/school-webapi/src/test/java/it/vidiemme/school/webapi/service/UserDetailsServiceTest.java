package it.vidiemme.school.webapi.service;

import static org.junit.Assert.assertNotNull;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import it.vidiemme.school.webapi.config.JUnitTestBase;

/**
 * Tests basic DB features like user found and user not found expected behaviors
 * 
 * These tests rely on an in-memory H2 database. See configuration jdbc.properties in src/test/resources/properties
 * and the database DDLs and SQL initialization stored in src/test/resources/h2-bootstrap. 
 * The datasource bean is defined in TestRepositoryConfig
 * 
 * @author flavio.ricci
 */
@DisplayName("Database related features")
@Tag("DB")
@ExtendWith(SpringExtension.class)
public class UserDetailsServiceTest extends JUnitTestBase {

	@Autowired
	private UserDetailsService userDetailsService;

	@Test
	public void whenLoggingInExistingUser_thenReturnedUserIsFound() {
		UserDetails userDetails = userDetailsService.loadUserByUsername("test_viewer@vidiemme.it");
		assertNotNull(userDetails);
	}
	
	@Test
	public void whenLoggingInExistingUser_thenUsernameNotFoundExceptionIsThrown() {
		Assertions.assertThrows(UsernameNotFoundException.class, () -> {
			userDetailsService.loadUserByUsername("NOT_EXISTING_USER@vidiemme.it");
	    });
	}

}
