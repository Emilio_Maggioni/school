<%@include file="/includes/taglibs.jsp"%>
<head>
    <title><spring:message code="example.section"/> <spring:message code="project.brand"/></title>
</head>
<body>
    <c:set var="currentMenuItem" scope="request" value="example-section"/>

    <div class="row border-bottom white-bg page-title">
        <div class="col-lg-10">
            <h2><spring:message code="example.section"/></h2>
            <ol class="breadcrumb">
                <li class="active breadcrumb-item">
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-12">
                <div class="ibox" >
                    <div class="ibox-title">
                        <h5><spring:message code="example.section"/></h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>

</body>
