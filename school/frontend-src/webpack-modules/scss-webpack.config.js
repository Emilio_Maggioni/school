const path = require('path');
const pathConfig = require('./path-webpack.config');

module.exports = (project, platform) => {
    const webadminsrc = [pathConfig.SOURCE_DIR, 'src',
        project,
    ].join(path.sep);

    switch (project){
    case 'webadmin':
        if (platform === 'back-office') {
            return [
                path.resolve(webadminsrc, 'vendor', 'css', 'bootstrap.min.css'),
                path.resolve(webadminsrc, 'vendor','styles','font-awesome','scss','font-awesome.scss'),
                path.resolve(webadminsrc, 'vendor','css', 'toastr.min.css'),
                path.resolve(webadminsrc, 'vendor','css', 'animate.css'),
                path.resolve(webadminsrc, 'vendor','styles','inspinia-template','style.scss'),
                path.resolve(webadminsrc, 'vendor','styles','chosen','bootstrap-chosen.css'),
                path.resolve(webadminsrc, 'vendor','css', 'dataTables.bootstrap4.min.css'),
                path.resolve(webadminsrc, 'vendor', 'css', 'select.bootstrap4.min.css'),
            ];
        }
        return [];
    default:
        return [];
    }
};
