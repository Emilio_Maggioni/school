import { baseURL, localStoragePrefix } from 'config';

/**
 * Base Repository
 *
 * @param {string} uri URI da concatenare al `baseURL` dichiarato nelle configurazioni
 */
class BaseRepository {
    constructor(uri = '') {
        this.baseURL = baseURL;
        this.uri = uri;
        this.URL = this.baseURL + this.uri;

        this.localStoragePrefix = localStoragePrefix;
    }
}

export default BaseRepository;
