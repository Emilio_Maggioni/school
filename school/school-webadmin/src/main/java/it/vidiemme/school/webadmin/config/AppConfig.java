package it.vidiemme.school.webadmin.config;

import java.util.Properties;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.context.support.PropertySourcesPlaceholderConfigurer;
import org.springframework.context.support.ReloadableResourceBundleMessageSource;
import org.springframework.core.env.Environment;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.JavaMailSenderImpl;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.MapperFeature;
import com.fasterxml.jackson.databind.ObjectMapper;

/**
 * Creates the main application beans, and scans for the other configuration classes in the package "config".
 */
@Configuration
@PropertySource({ "classpath:properties/app.properties" })
@PropertySource({ "classpath:properties/mail.properties" })
@ComponentScan(basePackages = { "it.vidiemme.school.webadmin.config" })
public class AppConfig {

    
    /**
     * The Environment variable provides a convenient service interface to resolve all the system properties, 
     * coming from different sources (properties files, JVM system properties, environment variables, etc.).
     */
    @Autowired
    private Environment env;

    /** The bean resolves ${...} placeholders in @Value and @PropertySource annotations against the current Spring Environment and its set of PropertySources.
     * @return the PropertySourcesPlaceholderConfigurer bean
     */
    @Bean
    public static PropertySourcesPlaceholderConfigurer propertySourcesPlaceholderConfigurer() {
        PropertySourcesPlaceholderConfigurer propertyConfigurer = new PropertySourcesPlaceholderConfigurer();
        propertyConfigurer.setIgnoreUnresolvablePlaceholders(true);
        return propertyConfigurer;
    }

    /** The bean resolves the localized application messages, reading the resource files in the "locale" folder.
     * @param cacheSeconds the number of seconds to cache loaded properties files
     * @return the MessageSource bean
     */
    @Bean
    public MessageSource messageSource(@Value("${messageSource.cacheSeconds}") Integer cacheSeconds) {
        ReloadableResourceBundleMessageSource messageSource = new ReloadableResourceBundleMessageSource();
        messageSource.setBasename("classpath:locale/messages");
        messageSource.setUseCodeAsDefaultMessage(true);
        messageSource.setDefaultEncoding("UTF-8");
        messageSource.setCacheSeconds(cacheSeconds);
        return messageSource;
    }

    /** The bean sends the application emails, using the configuration defined in the mail.properties file.
     * @return the JavaMailSender bean
     */
    @Bean
    public JavaMailSender getJavaMailSender() {
        JavaMailSenderImpl mailSender = new JavaMailSenderImpl();
        mailSender.setHost(env.getProperty("mail.smtp.host"));
        mailSender.setPort(Integer.parseInt(env.getProperty("mail.smtp.port")));
        mailSender.setUsername(env.getProperty("mail.smtp.username"));
        mailSender.setPassword(env.getProperty("mail.smtp.password"));
        mailSender.setProtocol(env.getProperty("mail.smtp.protocol"));

        Properties javaMailProperties = new Properties();
        javaMailProperties.setProperty("mail.smtps.auth", env.getProperty("mail.smtps.auth"));
        javaMailProperties.setProperty("mail.smtp.ssl.enable", env.getProperty("mail.smtp.ssl.enable"));
        javaMailProperties.setProperty("mail.smtp.protocol", env.getProperty("mail.smtp.protocol"));
        mailSender.setJavaMailProperties(javaMailProperties);
        return mailSender;
    }
    
    /** The bean manages the conversion between Java Objects and JSON Strings
     * @return the ObjectMapper bean
     */
    @Bean
    public ObjectMapper objectMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        mapper.configure(MapperFeature.DEFAULT_VIEW_INCLUSION, true);
        return mapper;
    }
}
