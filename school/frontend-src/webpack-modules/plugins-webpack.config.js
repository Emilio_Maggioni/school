const path = require('path');
const webpack = require('webpack');
const ConcatPlugin = require('webpack-concat-plugin');
const CopyWebpackPlugin = require('copy-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const HandlebarsPlugin = require("handlebars-webpack-plugin");
const packageJSON = require('../package');
const pathConfig = require('./path-webpack.config');

const vendorWebadminPath = path.resolve(pathConfig.SOURCE_DIR,'src','webadmin','vendor','js');
/**
 * File js del tema che vanno importati o plugin necessari
 * @type {Array}
 */
const themeJS = {
    webadmin: {
        'back-office': [
            path.resolve(vendorWebadminPath,'i18next.min.js'),

            path.resolve(vendorWebadminPath, 'jquery-3.4.1.min.js'),

            path.resolve(vendorWebadminPath, 'bootstrap.min.js'),
            path.resolve(vendorWebadminPath, 'bootbox.min.js'),

            path.resolve(vendorWebadminPath, 'popper.min.js'),
            path.resolve(vendorWebadminPath, 'inspinia.js'),
            path.resolve(vendorWebadminPath, 'pace.min.js'),


            path.resolve(vendorWebadminPath, 'jquery.slimscroll.min.js'),
            path.resolve(vendorWebadminPath, 'jquery.metisMenu.js'),

            path.resolve(vendorWebadminPath, 'chosen.jquery.js'),

            path.resolve(vendorWebadminPath, 'jquery.dataTables.min.js'),
            path.resolve(vendorWebadminPath, 'dataTables.bootstrap4.min.js'),
            path.resolve(vendorWebadminPath, 'dataTables.select.min.js'),

            path.resolve(vendorWebadminPath, 'jquery.serialize-object.min.js'),

            path.resolve(vendorWebadminPath,'toastr.min.js'),
        ],
        'front-office': [],
    },
};

const localesWebadminPath = path.resolve(pathConfig.SOURCE_DIR, 'src', 'webadmin', 'locales');
/**
 * File js per la localizzazione delle stringhe nei file javascript
 * @type {Array}
 */
let localesJS = {
    'webadmin': {
        'back-office': [
            path.resolve(localesWebadminPath, 'it.js'),
            path.resolve(localesWebadminPath, 'en.js'),
        ],
        'front-office': []
    }
};

/**
 * File js custom che vanno importati
 *
 * @type {Array}
 */
const webadminJSPath = path.resolve(pathConfig.SOURCE_DIR, 'src', 'webadmin', 'javascripts');
const customJs = {
    webadmin: {
        'back-office': [
            path.resolve(webadminJSPath, 'functions.js'),
            path.resolve(webadminJSPath, 'utils.js'),
        ],
        'front-office': [],
    },
};


const getPlugins = (PROFILE, project, platform) => {
    const plugins = [];

    const SOURCE_DIR = path.resolve(pathConfig.SOURCE_DIR, 'src', project);

    const BUILD_DIR = path.resolve(pathConfig.BUILD_DIR, '..',
        `${packageJSON.name}-${project}`,
        packageJSON.project_config.projects[project].build_path.join(path.sep));

    // APP REACT
    // const APP_DIR = path.resolve(SOURCE_DIR, project, 'src');


    const cleanPlugin = new CleanWebpackPlugin();

    // Concatenazione file js
    const jsToConcat = themeJS[project][platform].concat(localesJS[project][platform]).concat(customJs[project][platform]);


    /**
     * Copia dei file statici
     *
     * @type {CopyWebpackPlugin}
     */
    const copyStatics = new CopyWebpackPlugin([
        {
            from: path.resolve(SOURCE_DIR, 'static'),
            to: path.resolve(BUILD_DIR, 'static'),
            ignore: ['.gitkeep'],
        },
    ]);

    /**
     * Concatenazione dei file JS custom e del tema
     *
     * @type {ConcatPlugin}
     */
    let concatJs = null;
    if (jsToConcat.length > 0) {
        concatJs = new ConcatPlugin({
            uglify: PROFILE === 'staging' || PROFILE === 'production',
            sourceMap: !(PROFILE === 'staging' || PROFILE === 'production'),
            name: `${platform}-index`,
            outputPath: '',
            fileName: '[name].js',
            filesToConcat: jsToConcat,
        });
    }

    /**
     * Render template handlebars
     *
     * @type {CopyWebpackPlugin}
     */
    const copySamples = new HandlebarsPlugin({
        // path to hbs entry file(s)
        htmlWebpackPlugin: {
            enabled: true, // register all partials from html-webpack-plugin, defaults to `false`
            prefix: 'html', // where to look for htmlWebpackPlugin output. default is "html"
        },
        entry: path.join(SOURCE_DIR, 'samples', 'pages', '*.hbs'),
        // output path and filename(s). This should lie within the webpacks output-folder
        // if ommited, the input filepath stripped of its extension will be used
        output: path.join(BUILD_DIR, 'samples', '[name].html'),
        // // data passed to main hbs template: `main-template(data)`
        // data: require("./app/data/project.json"),
        // // or add it as filepath to rebuild data on change using webpack-dev-server
        // data: path.join(__dirname, "app/data/project.json"),

        // globbed path to partials, where folder/filename is unique
        partials: [
            path.join(SOURCE_DIR, 'samples', 'partials', '*', '*.hbs'),
            // path.join(SOURCE_DIR, "samples", "partials", "layouts", "*.hbs")
        ],

        // register custom helpers. May be either a function or a glob-pattern
        helpers: {
        //    nameOfHbsHelper: Function.prototype,
        },
    });


    const envConfig = new webpack.DefinePlugin({
        'process.env': {
            NODE_ENV: JSON.stringify(PROFILE),
            __DEVTOOLS__: (PROFILE === 'local' || PROFILE === 'development'),
        },
        VERSION: JSON.stringify(packageJSON.version),
    });

    const HtmlWebpackPluginConfig = new HtmlWebpackPlugin({
        template: path.resolve(SOURCE_DIR, 'src', platform, 'index.html'),
        title: packageJSON.displayName || 'Vidiemme Consulting s.r.l.',
        favicon: path.resolve(SOURCE_DIR, 'src', platform, 'favicon.png'),
        filename: `${platform}-index.html`,
        inject: 'body',
        minify: {
            collapseWhitespace: true,
            collapseInlineTagWhitespace: true,
            removeComments: true,
            removeRedundantAttributes: true,
        },
    });

    plugins.push(cleanPlugin);
    plugins.push(envConfig);

    plugins.push(new MiniCssExtractPlugin({
        filename: '[name].css',
        chunkFilename: '[id]-[hash].css',
    }));

    plugins.push(copyStatics);
    plugins.push(concatJs);
    if (PROFILE === 'local') {
        plugins.push(HtmlWebpackPluginConfig);
        plugins.push(copySamples);
    }


    return plugins;
};

module.exports = (PROFILE, project, platform) => getPlugins(PROFILE, project, platform);
