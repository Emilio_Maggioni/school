i18next.on('initialized', () => {
    i18next.addResourceBundle('en', 'boost', {
        errors: {
            networkError: "Network error",
        },
    }, true, true);

    i18next.addResourceBundle('en', 'boost', {
        dialogs: {
            error: "Error",
            warning: "Warning",
            confirm: "Confirm",
            cancel: "Cancel",
        },
    }, true, true);

    i18next.addResourceBundle('en', 'boost', {
        welcome: {
            welcome: "Welcome {{name}}!",
            randomizeName: "Randomize name",
        },
    }, true, true);
});
