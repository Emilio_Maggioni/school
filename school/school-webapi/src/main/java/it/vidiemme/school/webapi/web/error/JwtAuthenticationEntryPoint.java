package it.vidiemme.school.webapi.web.error;

import java.io.IOException;
import java.io.Serializable;
import java.util.Collections;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.security.web.AuthenticationEntryPoint;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.databind.ObjectMapper;

@Component
public class JwtAuthenticationEntryPoint implements AuthenticationEntryPoint, Serializable {

    private static final long serialVersionUID = 7180341086367179388L;
    private static final Logger logger = LoggerFactory.getLogger(JwtAuthenticationEntryPoint.class);

    @Autowired
    private ObjectMapper objectMapper;
    
    @Override
    public void commence(HttpServletRequest request, HttpServletResponse response, AuthenticationException authException) throws IOException {
        logger.error(String.format("Error in request coming from host addr %s ; name %s", request.getRemoteAddr(), request.getRemoteHost()));
        logger.error(String.format("User not authorized while calling [%s] %s; user not authorized or maybe JWT token expired", request.getMethod(), request.getRequestURI()));
    	
        List<ApiGlobalError> apiGlobalErrors = Collections.singletonList(new ApiGlobalError(authException.getMessage(), null));
        ApiErrorResponse apiErrorResponse = new ApiErrorResponse(null, apiGlobalErrors);
        ResponseEntity<ApiErrorResponse> resp = new ResponseEntity<>(apiErrorResponse, HttpStatus.UNAUTHORIZED);
        
        //response.sendError(HttpServletResponse.SC_UNAUTHORIZED, "Unauthorized");
        response.setContentType(MediaType.APPLICATION_JSON);
        response.setStatus(HttpStatus.UNAUTHORIZED.value());
        response.getOutputStream().println(objectMapper.writeValueAsString(resp.getBody()));
    }
    
}
