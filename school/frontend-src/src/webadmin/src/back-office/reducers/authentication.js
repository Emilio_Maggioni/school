import { SIGNIN, SIGNOUT } from '../actions/authentication';

/**
 * Reducer che gestisce lo stato dell'autenticazione
 *
 * @param {Object} state Lo stato dell'autenticazione
 * @param {Object} action
 * @returns {Object} Lo stato aggiornato
 */
export default function authentication(state = { loading: false, authenticated: false }, action) {
    switch (action.type) {
    case SIGNIN:
        switch (action.status) {
        case 'loading':
            return {
                loading: true,
            };
        case 'success':
            return {
                loading: false,
                authenticated: true,
            };
        default:
            return {
                loading: false,
                authenticated: false,
            };
        }
    case SIGNOUT:
        switch (action.status) {
        case 'loading':
            return {
                loading: true,
            };
        case 'success':
            return {
                loading: false,
                authenticated: false,
            };
        default:
            return {
                loading: false,
                authenticated: false,
            };
        }
    default:
        return state;
    }
}
