<?xml version="1.0" encoding="UTF-8"?>
<project>

	<modelVersion>4.0.0</modelVersion>
	<groupId>it.vidiemme</groupId>
	<artifactId>school</artifactId>
	<version>0.0.1-SNAPSHOT</version>
	<packaging>pom</packaging>
	<name>school</name>
    
    <properties>
		<project.build.sourceEncoding>UTF-8</project.build.sourceEncoding>
		<project.reporting.outputEncoding>UTF-8</project.reporting.outputEncoding>
		<spring.version>5.0.4.RELEASE</spring.version>
		<spring.security.version>5.0.3.RELEASE</spring.security.version>
		<cucumber.version>1.2.5</cucumber.version>
		<junit-platform.version>1.3.1</junit-platform.version>
		<junit-jupiter.version>5.3.1</junit-jupiter.version>
	</properties>

    <build>
        <pluginManagement>
            <plugins>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-compiler-plugin</artifactId>
                    <version>3.8.0</version>
                    <configuration>
                        <source>1.8</source>
                        <target>1.8</target>
                        <encoding>UTF-8</encoding>
                        <useIncrementalCompilation>false</useIncrementalCompilation>
                    </configuration>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-war-plugin</artifactId>
                    <version>3.2.0</version>
                    <configuration>
                        <dependentWarExcludes>**/web.xml,WEB-INF/classes/META-INF/**</dependentWarExcludes>
                    </configuration>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-release-plugin</artifactId>
                    <version>2.5.1</version>
                    <dependencies>
                        <dependency>
                            <groupId>org.apache.maven.shared</groupId>
                            <artifactId>maven-invoker</artifactId>
                            <version>2.2</version>
                        </dependency>
                    </dependencies>
                </plugin>
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-archetype-plugin</artifactId>
                    <version>3.1.0</version>
                    <executions>
                        <execution>
                            <id>generate-archetype-install</id>
                            <phase>install</phase>
                            <configuration>
                                <archetypePostPhase>install</archetypePostPhase>
                            </configuration>
                            <goals>
                                <goal>create-from-project</goal>
                            </goals>
                        </execution>
                    </executions>
                    <configuration>
                        <outputDirectory>../archetype</outputDirectory>
                        <propertyFile>archetype_params.properties</propertyFile>
                        <preserveCData>false</preserveCData>
                        <defaultEncoding>UTF-8</defaultEncoding>
                    </configuration>
                </plugin>
                <!-- Avoid filtering (placeholders replacement applied using 
                    profile properties) for the specified file extensions -->
                <plugin>
                    <groupId>org.apache.maven.plugins</groupId>
                    <artifactId>maven-resources-plugin</artifactId>
                    <version>3.0.2</version>
                    <configuration>
                        <includeEmptyDirs>true</includeEmptyDirs>
                        <nonFilteredFileExtensions>
                            <nonFilteredFileExtension>p12</nonFilteredFileExtension>
                        </nonFilteredFileExtensions>
                        <encoding>UTF-8</encoding>
                    </configuration>
                </plugin>
                <plugin>
                    <artifactId>maven-deploy-plugin</artifactId>
                    <version>2.8.2</version>
                </plugin>
             </plugins>
        </pluginManagement>
        
        <plugins>
	        <!-- avoid the infinite loop using eclipse build, caused by the frontend-maven-plugin -->
	         <plugin>
	             <groupId>org.eclipse.m2e</groupId>
	             <artifactId>lifecycle-mapping</artifactId>
	             <version>1.0.0</version>
	             <configuration>
	                 <lifecycleMappingMetadata>
	                    <pluginExecutions>
	                         <pluginExecution>
	                             <pluginExecutionFilter>
	                                 <groupId>com.github.eirslett</groupId>
	                                 <artifactId>frontend-maven-plugin</artifactId>
	                                 <versionRange>[0.0.23,)</versionRange>
	                                 <goals>
	                                     <goal>npm</goal>
	                                     <goal>bower</goal>
	                                     <goal>gulp</goal>
	                                 </goals>
	                             </pluginExecutionFilter>
	                             <action>
	                                <execute>
	                                  <runOnIncremental>false</runOnIncremental>
	                                </execute>
	                              </action>
	                         </pluginExecution>
	                     </pluginExecutions>
	                 </lifecycleMappingMetadata>
	             </configuration>
	         </plugin>
	         
        </plugins>

        <resources>
            <resource>
                <directory>src/main/resources</directory>
                <filtering>true</filtering>
            </resource>
        </resources>
        <testResources>
            <testResource>
                <directory>src/test/resources</directory>
                <filtering>true</filtering>
            </testResource>
        </testResources>

    </build>

	<dependencyManagement>
		<dependencies>

		    <dependency>
		      <groupId>javax.servlet</groupId>
		      <artifactId>javax.servlet-api</artifactId>
		      <version>3.1.0</version>
		      <scope>provided</scope>
		    </dependency>	
	        <dependency>
	            <groupId>javax.ws.rs</groupId>
	            <artifactId>javax.ws.rs-api</artifactId>
	            <version>2.0</version>
	        </dependency>
	        <dependency>
	           <groupId>org.glassfish</groupId>
	           <artifactId>javax.el</artifactId>
	           <version>3.0.0</version>
	           <scope>provided</scope>
	        </dependency>	        

			<!-- Spring -->

			<dependency>
				<groupId>org.springframework</groupId>
				<artifactId>spring-context</artifactId>
				<version>${spring.version}</version>
			</dependency>
			<dependency>
				<groupId>org.springframework</groupId>
				<artifactId>spring-web</artifactId>
				<version>${spring.version}</version>
			</dependency>
			<dependency>
				<groupId>org.springframework</groupId>
				<artifactId>spring-webmvc</artifactId>
				<version>${spring.version}</version>
			</dependency>
			<dependency>
				<groupId>org.springframework</groupId>
				<artifactId>spring-aop</artifactId>
				<version>${spring.version}</version>
			</dependency>
			<dependency>
				<groupId>org.springframework</groupId>
				<artifactId>spring-jdbc</artifactId>
				<version>${spring.version}</version>
			</dependency>
			<dependency>
				<groupId>org.springframework</groupId>
				<artifactId>spring-context-support</artifactId>
				<version>${spring.version}</version>
			</dependency>
			<dependency>
				<groupId>org.springframework</groupId>
				<artifactId>spring-test</artifactId>
				<version>${spring.version}</version>
				<scope>test</scope>
			</dependency>

			<!-- Tests -->
			<dependency>
	            <groupId>org.junit.jupiter</groupId>
	            <artifactId>junit-jupiter-api</artifactId>
	            <version>${junit-jupiter.version}</version>
	            <scope>test</scope>
	        </dependency>
			<dependency>
	            <groupId>org.junit.jupiter</groupId>
	            <artifactId>junit-jupiter-engine</artifactId>
	            <version>${junit-jupiter.version}</version>
	            <scope>test</scope>
	        </dependency>
	        <dependency>
	            <groupId>org.junit.vintage</groupId>
	            <artifactId>junit-vintage-engine</artifactId>
	            <version>${junit-jupiter.version}</version>
	            <scope>test</scope>
	        </dependency>
	        <dependency>
	            <groupId>org.junit.platform</groupId>
	            <artifactId>junit-platform-launcher</artifactId>
	            <version>${junit-platform.version}</version>
	            <scope>test</scope>
	        </dependency>
	        <dependency>
	            <groupId>org.junit.platform</groupId>
	            <artifactId>junit-platform-runner</artifactId>
	            <version>${junit-platform.version}</version>
	            <scope>test</scope>
	        </dependency>
			<dependency>
				<groupId>org.mockito</groupId>
				<artifactId>mockito-core</artifactId>
				<version>2.23.0</version>
				<scope>test</scope>
			</dependency>
			<dependency>
			    <groupId>org.mockito</groupId>
			    <artifactId>mockito-junit-jupiter</artifactId>
			    <version>2.23.0</version>
			    <scope>test</scope>
			</dependency>
			<dependency>
			    <groupId>com.h2database</groupId>
			    <artifactId>h2</artifactId>
			    <version>1.4.197</version>
			    <scope>test</scope>
			</dependency>

			<!-- Cucumber tests -->
			<dependency>
				<groupId>info.cukes</groupId>
				<artifactId>cucumber-java</artifactId>
				<version>${cucumber.version}</version>
				<scope>test</scope>
			</dependency>
			<dependency>
				<groupId>info.cukes</groupId>
				<artifactId>cucumber-junit</artifactId>
				<version>${cucumber.version}</version>
				<scope>test</scope>
			</dependency>
			<dependency>
				<groupId>info.cukes</groupId>
				<artifactId>cucumber-spring</artifactId>
				<version>${cucumber.version}</version>
				<scope>test</scope>
			</dependency>

			<!-- Spring Security -->
			<dependency>
				<groupId>org.springframework.security</groupId>
				<artifactId>spring-security-web</artifactId>
				<version>${spring.security.version}</version>
			</dependency>
			<dependency>
				<groupId>org.springframework.security</groupId>
				<artifactId>spring-security-taglibs</artifactId>
				<version>${spring.security.version}</version>
			</dependency>
			<dependency>
				<groupId>org.springframework.security</groupId>
				<artifactId>spring-security-config</artifactId>
				<version>${spring.security.version}</version>
			</dependency>
			<dependency>
				<groupId>org.springframework.security</groupId>
				<artifactId>spring-security-core</artifactId>
				<version>${spring.security.version}</version>
			</dependency>

			<!-- Hibernate Validator -->
			<dependency>
				<groupId>org.hibernate</groupId>
				<artifactId>hibernate-validator</artifactId>
				<version>6.0.3.Final</version>
			</dependency>
            <dependency>
               <groupId>javax.el</groupId>
               <artifactId>javax.el-api</artifactId>
               <version>3.0.0</version>
            </dependency>

			<!-- Log -->

			<dependency>
				<groupId>ch.qos.logback</groupId>
				<artifactId>logback-classic</artifactId>
				<version>1.2.3</version>
			</dependency>
			<dependency>
				<groupId>org.codehaus.groovy</groupId>
				<artifactId>groovy-all</artifactId>
				<version>2.4.13</version>
			</dependency>
	        <dependency>
	            <groupId>org.slf4j</groupId>
	            <artifactId>jcl-over-slf4j</artifactId>
	            <version>1.7.26</version>
	        </dependency>

			<!-- Database -->

			<dependency>
				<groupId>org.mybatis</groupId>
				<artifactId>mybatis-spring</artifactId>
				<version>1.3.1</version>
			</dependency>
			<dependency>
				<groupId>org.mybatis</groupId>
				<artifactId>mybatis</artifactId>
				<version>3.4.5</version>
			</dependency>
			<dependency>
				<groupId>mysql</groupId>
				<artifactId>mysql-connector-java</artifactId>
				<version>5.1.45</version>
			</dependency>
			<dependency>
				<groupId>org.apache.commons</groupId>
				<artifactId>commons-dbcp2</artifactId>
				<version>2.2.0</version>
			</dependency>

			<!-- Other libraries -->

			<dependency>
				<groupId>commons-fileupload</groupId>
				<artifactId>commons-fileupload</artifactId>
				<version>1.3.3</version>
			</dependency>
			<dependency>
				<groupId>commons-beanutils</groupId>
				<artifactId>commons-beanutils</artifactId>
				<version>1.9.3</version>
			</dependency>
			<dependency>
				<groupId>commons-lang</groupId>
				<artifactId>commons-lang</artifactId>
				<version>2.6</version>
			</dependency>
			<dependency>
				<groupId>com.sun.mail</groupId>
				<artifactId>javax.mail</artifactId>
				<version>1.6.1</version>
			</dependency>
	        <dependency>
	            <groupId>com.google.guava</groupId>
	            <artifactId>guava</artifactId>
	            <version>27.0.1-jre</version>
	        </dependency>

			<!-- Thymeleaf -->
			<dependency>
				<groupId>org.thymeleaf</groupId>
				<artifactId>thymeleaf</artifactId>
				<version>3.0.9.RELEASE</version>
			</dependency>
			<dependency>
				<groupId>org.thymeleaf</groupId>
				<artifactId>thymeleaf-spring5</artifactId>
				<version>3.0.9.RELEASE</version>
			</dependency>

			<!-- Sitemesh decorator -->
			<!-- https://mvnrepository.com/artifact/org.sitemesh/sitemesh -->
			<dependency>
				<groupId>org.sitemesh</groupId>
				<artifactId>sitemesh</artifactId>
				<version>3.0.0</version>
			</dependency>

			<!-- https://mvnrepository.com/artifact/com.fasterxml.jackson.core/jackson-databind -->
			<dependency>
				<groupId>com.fasterxml.jackson.core</groupId>
				<artifactId>jackson-databind</artifactId>
				<version>2.9.7</version>
			</dependency>

			<!-- JSTL -->
			<dependency>
				<groupId>jstl</groupId>
				<artifactId>jstl</artifactId>
				<version>1.2</version>
			</dependency>

			<!-- JWT -->
			<dependency>
				<groupId>io.jsonwebtoken</groupId>
				<artifactId>jjwt</artifactId>
				<version>0.9.0</version>
			</dependency>

			<!-- HTTP client -->
			<dependency>
				<groupId>org.apache.httpcomponents</groupId>
				<artifactId>httpclient</artifactId>
				<version>4.5.6</version>
			</dependency>

			<!-- JSON object -->
			<dependency>
				<groupId>org.json</groupId>
				<artifactId>json</artifactId>
				<version>20180813</version>
			</dependency>

		</dependencies>
	</dependencyManagement>

	<dependencies>
	
        <dependency>
          <groupId>javax.servlet</groupId>
          <artifactId>javax.servlet-api</artifactId>
        </dependency>				

		<!-- Spring -->
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-context</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-web</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-jdbc</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-context-support</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-webmvc</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-aop</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework</groupId>
			<artifactId>spring-test</artifactId>
			<scope>test</scope>
		</dependency>

		<!-- Spring security -->
		<dependency>
			<groupId>org.springframework.security</groupId>
			<artifactId>spring-security-web</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.security</groupId>
			<artifactId>spring-security-taglibs</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.security</groupId>
			<artifactId>spring-security-config</artifactId>
		</dependency>
		<dependency>
			<groupId>org.springframework.security</groupId>
			<artifactId>spring-security-core</artifactId>
		</dependency>
		
        <!-- Hibernate Validator -->
        <dependency>
            <groupId>org.hibernate</groupId>
            <artifactId>hibernate-validator</artifactId>
        </dependency>
        
        <!-- Thymeleaf -->
        <dependency>
            <groupId>org.thymeleaf</groupId>
            <artifactId>thymeleaf</artifactId>
        </dependency>
        <dependency>
            <groupId>org.thymeleaf</groupId>
            <artifactId>thymeleaf-spring5</artifactId>
        </dependency>

		<!-- Log -->
		<dependency>
			<groupId>ch.qos.logback</groupId>
			<artifactId>logback-classic</artifactId>
		</dependency>
		<dependency>
			<groupId>org.codehaus.groovy</groupId>
			<artifactId>groovy-all</artifactId>
		</dependency>
        <dependency>
            <groupId>org.slf4j</groupId>
            <artifactId>jcl-over-slf4j</artifactId>
        </dependency>

		<!-- Jackson -->
		<dependency>
			<groupId>com.fasterxml.jackson.core</groupId>
			<artifactId>jackson-databind</artifactId>
		</dependency>

		<!-- Other libraries -->
		<dependency>
			<groupId>com.sun.mail</groupId>
			<artifactId>javax.mail</artifactId>
		</dependency>
        <dependency>
            <groupId>commons-fileupload</groupId>
            <artifactId>commons-fileupload</artifactId>
        </dependency>
        <dependency>
            <groupId>commons-lang</groupId>
            <artifactId>commons-lang</artifactId>
        </dependency>        

		<!-- Tests -->
		<dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-api</artifactId>
            <scope>test</scope>
        </dependency>
		<dependency>
            <groupId>org.junit.jupiter</groupId>
            <artifactId>junit-jupiter-engine</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.junit.vintage</groupId>
            <artifactId>junit-vintage-engine</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.junit.platform</groupId>
            <artifactId>junit-platform-launcher</artifactId>
            <scope>test</scope>
        </dependency>
        <dependency>
            <groupId>org.junit.platform</groupId>
            <artifactId>junit-platform-runner</artifactId>
            <scope>test</scope>
        </dependency>
		<dependency>
			<groupId>org.mockito</groupId>
			<artifactId>mockito-core</artifactId>
			<scope>test</scope>
		</dependency>
		<dependency>
		    <groupId>org.mockito</groupId>
		    <artifactId>mockito-junit-jupiter</artifactId>
		    <scope>test</scope>
		</dependency>
		<dependency>
			<groupId>info.cukes</groupId>
			<artifactId>cucumber-java</artifactId>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>info.cukes</groupId>
			<artifactId>cucumber-junit</artifactId>
			<scope>test</scope>
		</dependency>
		<dependency>
			<groupId>info.cukes</groupId>
			<artifactId>cucumber-spring</artifactId>
			<scope>test</scope>
		</dependency>
		<dependency>
		    <groupId>com.h2database</groupId>
		    <artifactId>h2</artifactId>
		    <scope>test</scope>
		</dependency>
        <dependency>
           <groupId>org.glassfish</groupId>
           <artifactId>javax.el</artifactId>
        </dependency>		

	</dependencies>

  <modules>
    <module>school-business</module>
    <module>school-webadmin</module>
    <module>school-webapi</module>
  </modules>
</project>