import AjaxRequest from './AjaxRequest';

class JSONRequest extends AjaxRequest {
    send() {
        return super.send()
            .then(data => ({
                status: data.status,
                code: data.code,
                data: JSON.parse(data.data || null),
                xhr: data.xhr,
            }))
            .catch((error) => {
                throw error;
            });
    }
}

export default JSONRequest;
