package it.vidiemme.school.webadmin.config;

import org.sitemesh.builder.SiteMeshFilterBuilder;
import org.sitemesh.config.ConfigurableSiteMeshFilter;
import org.sitemesh.content.tagrules.html.Sm2TagRuleBundle;

/** 
 * Configures the Sitemesh decorator engine.
 * For each route, a set of hierarchical jsp decorators are specified; the last decorator specified is the first to be applied.
 */
public class SitemeshFilterConfig  extends ConfigurableSiteMeshFilter {

    @Override
    protected void applyCustomConfiguration(SiteMeshFilterBuilder builder) {
        // @formatter:off
        builder.addDecoratorPaths("/*", "/decorators/main_menu.jsp", "/decorators/logged_user.jsp", "/decorators/default.jsp")
               .addDecoratorPaths("/403", "/decorators/default.jsp")
               .addDecoratorPaths("/404", "/decorators/default.jsp")
               .addDecoratorPaths("/500", "/decorators/default.jsp")
               .addDecoratorPaths("/login*", "/decorators/login.jsp", "/decorators/default.jsp")
               .addDecoratorPaths("/user/set-new-password*", "/decorators/login.jsp", "/decorators/default.jsp")
               .addDecoratorPaths("/user/reset-password*", "/decorators/login.jsp", "/decorators/default.jsp")
               .addDecoratorPaths("/user/change-password*", "/decorators/login.jsp", "/decorators/default.jsp")
               .addExcludedPath("/assets/*")
               .addExcludedPath("/decorators/*")
               .setIncludeErrorPages(true)
               .addTagRuleBundle(new Sm2TagRuleBundle());
        // @formatter:on
    }
}