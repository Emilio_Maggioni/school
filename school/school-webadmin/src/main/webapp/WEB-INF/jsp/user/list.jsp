<%@include file="/includes/taglibs.jsp"%>
<head>
    <title><spring:message code="user.manage"/> <spring:message code="project.brand"/></title>
</head>
<body>
    <c:set var="currentMenuItem" scope="request" value="manage-users"/>

    <a href="${ctx}/user/add" class="btn btn-primary float-action z-depth-2">
        <i class="fa fa-plus"></i>
    </a>

    <div class="row border-bottom white-bg page-title">
        <div class="col-lg-10">
            <h2><spring:message code="user.manage"/></h2>
            <ol class="breadcrumb">
                <li class="active breadcrumb-item">
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-12">
                <jsp:include page="/WEB-INF/jsp/success_message.jsp" />
                <spring:hasBindErrors name="filter">
                    <jsp:include page="/WEB-INF/jsp/error_messages.jsp" />
                </spring:hasBindErrors>
                <div class="ibox">
                    <div class="ibox-title">
                        <h5><spring:message code="user.list"/></h5>
                    </div>
                    <div class="ibox-content">
                        <form method="post" id="filter" class="filters-container">
                            <div class="row">
                                <div class="col-sm-2 form-group">
                                    <label><spring:message code="user.username"/></label>
                                    <input class="form-control" id="username" name="username" type="text" value="${filter.username}"/>
                                </div>
                                <div class="col-sm-2 form-group">
                                    <label><spring:message code="user.fullName"/></label>
                                    <input class="form-control" id="fullName" name="fullName" type="text" value="${filter.fullName}"/>
                                </div>
                                <div class="col-sm-2 form-group">
                                    <label><spring:message code="user.enabled"/></label>
                                    <select name="isActive" class="chosen-select form-control">
                                        <option value="" ><spring:message code="empty.select"/></option>
                                        <option value="true" ><spring:message code="user.enabled"/></option>
                                        <option value="false" ><spring:message code="user.disabled"/></option>
                                    </select>
                                </div>
                                <div class="col-sm-2 form-group">
                                    <label><spring:message code="user.roles"/></label>
                                    <select id="roleIds" name="roleIds[]" class="chosen-select form-control" multiple="multiple" data-placeholder=" ">
                                        <option label="---" value="" />
                                        <c:forEach items="${roleList}" var="role">
                                            <c:set value="" var="selected"></c:set>
                                            <c:forEach items="${filter.roleIds}" var="userRole">
                                                <c:if test="${role.id == userRole}">
                                                    <c:set value="selected" var="selected"></c:set>
                                                </c:if>
                                            </c:forEach>
                                            <option value="${role.id}" ${selected}><spring:message code="${role.name}" /></option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="col-sm-4 text-right">
                                    <button class="btn btn-primary" type="button">
                                        <i class="fa fa-search"></i> <spring:message code="search"/>
                                    </button>
                                </div>
                            </div>
                        </form>
	                    <div class="table-responsive">
	                        <table class="table table-striped table-bordered table-hover datatable-users" style="width:100%">
	                            <thead>
	                                <tr>
	                                    <th><spring:message code="user.username"/></th>
	                                    <th><spring:message code="user.firstName"/></th>
	                                    <th><spring:message code="user.lastName"/></th>
	                                    <th><spring:message code="user.enabled"/></th>
	                                    <th><spring:message code="actions"/></th>
	                                </tr>
	                            </thead>
	                        </table>
	                    </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>

<content tag="localscript">
    <script type="text/javascript">
        $(document).ready(function(){
            var datatable = $('.datatable-users').DataTable({
                processing: true,
                serverSide: true,
                searching: false,
                lengthChange: false,
                "language": {
                    "url": "https://cdn.datatables.net/plug-ins/1.10.16/i18n/Italian.json"
                },
                ajax: {
                    url: "${ctx}/user/pagination",
                    type: "POST",
                    contentType: 'application/json;charset=UTF-8',
                    data: function (param) {
                        return JSON.stringify($.extend({}, param, jsonFilter("filter")));
                    }
                },
                "columns": [
                {"data": "username"},
                {"data": "firstName"},
                {"data": "lastName"},
                {"data": "isEnabled", "orderable": false, "render": function (data) { return data ? "<spring:message code='yes'/>" : "<spring:message code='no'/>"; }
            },
            {"data": "id",
            "render": function ( data, type, row, meta ) {
                return '<div class="actions-wrapper"><a href="#" class="actions-open-button"><i class="fa fa-ellipsis-v"></i></a><ul class="actions-container z-depth-1">' +
                    '<li><a href="${ctx}/user/'+data+'/view"><spring:message code="show"/></a></li>'+
                    '<li><a href="${ctx}/user/'+data+'/edit"><spring:message code="edit"/></a></li>'+
                    '<li><a class="ajax-confirm-delete" href="#" data-confirmtext="<spring:message code="user.confirm.delete" javaScriptEscape="true"/>" data-deleteurl="${ctx}/user/'+data+'/delete"><spring:message code="delete"/></a></li>'+
                    '</ul></div></td>';
                },
                "orderable": false
            }
            ],
            order: [[ 1, "asc" ]]
        });

        $('.datatable-users tbody').on('click', '.ajax-confirm-delete', function(e) {
            e.preventDefault();
            var that = $(this);
            openConfirmDelete(that.data('confirmtext'), null, null, null, function() {
                $.ajax({
                    url: that.data('deleteurl'),
                    type: 'DELETE',
                    success: function() {datatable.ajax.reload();}
                })
            });
            return false;
        });

        $('#filter button').click(function(e) {datatable.ajax.reload();});

    });
</script>
</content>
