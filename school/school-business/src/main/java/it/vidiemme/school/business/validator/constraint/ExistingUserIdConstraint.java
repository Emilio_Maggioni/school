package it.vidiemme.school.business.validator.constraint;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import it.vidiemme.school.business.service.UserService;

@Component
public class ExistingUserIdConstraint implements ConstraintValidator<ExistingUserId, Integer> {

    @Autowired
    private UserService userService;

    @Override
    public boolean isValid(Integer userId, ConstraintValidatorContext context) {
        return userService.getById(userId, Boolean.TRUE) != null;
    }
}
