/**
 * Base URL utilizzato dai repository per effettuare le chiamate al server
 *
 * @ignore
 * @type {string}
 */
export const baseURL = 'http://localhost:8181';

/**
 * Prefisso utilizzato per le chiavi salvate in localStorage
 *
 * @ignore
 * @type {string}
 */
export const localStoragePrefix = '';
