<%@include file="/includes/taglibs.jsp"%>
<c:if test="${not empty errors.globalErrors}" var="testSV_KO">
    <div class="row">
        <div class="col-12">
            <div class="alert alert-danger alert-dismissable">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="fa fa-times"></i>
                </button>
                <c:forEach var="error" items="${errors.globalErrors}">
                    <spring:message message="${error}" /><br />
                </c:forEach>
            </div>
        </div>
    </div>
</c:if>
