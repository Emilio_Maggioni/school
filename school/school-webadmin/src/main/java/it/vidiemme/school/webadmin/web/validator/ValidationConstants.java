package it.vidiemme.school.webadmin.web.validator;

public interface ValidationConstants {
    public static final String FIELD_INVALID = "Invalid";
    public static final String FIELDS_MISMATCH = "Mismatch";
    public static final String FIELD_ALREADY_USED = "AlreadyUsed";
    public static final String FIELD_NOT_DIFFERENT = "NotDifferent";
}
