# Sviluppo frontend
## Indice
1. [Introduzione](#introduzione) 
2. [Concetti di base](#concetti-base)
3. [Setup](#setup)
4. [Linee guida sviluppo](#linee-guida)

## 1. Introduzione <a name="introduzione" ></a>
Questo documento descrive la tecnica utilizzata per impostare lo sviluppo 
frontend di un sito web backoffice.  
Questa soluzione attualmente è integrata in un progetto Java/Spring ma 
può essere utilizzata in qualsiasi framework server (es: Laravel).   


## Concetti di base <a name="concetti-base"></a>

Questa soluzione ha come due obbiettivi principali: 

- integrare delle componenti ReactJS all'interno di un progetto.
- definire delle regole e un impostazione più ordinata per lo sviluppo 
della parte frontend (js, css). 


#### Non una single-page web app
Il fatto di utilizzare ReactJs per la parte di frontend __NON__ obbliga lo
sviluppatore a realizzare una single-page web app. La parte react infatti
può essere invocata in qualsiasi momento e in qualsiasi punto di una pagina
HTML "classica".  
Il vantaggio principale è quello di delegare alla parte server la questione 
del routing, la composizione delle pagine e il controllo immediato dei 
permessi dell'utente.  
Non si obbliga quindi a scrivere tutte le operazioni come API Rest.  
La parte server quindi si occupa di renderizzare lo scheletro della pagina,
quando è necessaria una parte con diverse interazioni dell'utente e una 
resa grafica maggiore può includere il componente react che implementa la feature.

#### Quando utilizzare una componente ReactJS

Le componenti react vengono utilizzate per le parti di interfaccia più complicate e che richiedono maggiori iterazioni dell'utante rispetto alle più classiche componenti Javascript (es datepicker, chosen select, jquery datatables).  
Alcuni esempi di componenti che è meglio sviluppare in react:

- Un sito che ha necessità di tenere dati in memoria ed effettuare dei calcoli e/o grafici con i dati raccolti e poi effettuare una sincronizzazione con il server.
- Il server impiega molto tempo per estrarre ed elaborare i dati richiesti dalla pagina. Quindi è consigliabile separare il tutto con delle chiamate a servizi REST e impaginare i risultati utilizzando componenti React.
- Sono richieste delle funzionalità che non possono essere sviluppate in modo diverso, oppure in maniera ordinata e mantenibile. Es: wizard molto complessi, player audio in background, utilizzo di web-socket per chat.

Per ogni singola componente che verrà sviluppata all'interno del sito bisognerà scegliere come svilupparla e come verrà resa disponibile alla parte server.  

#### Template inspinia

Per lo sviluppo dei siti backoffice andremo ad utilizzare il template [inspinia](https://wrapbootstrap.com/theme/inspinia-responsive-admin-theme-WB0R5L90S).  
Sia le componenti "classiche" sia le componenti react dovranno rispettare lo stile del template, a meno di direttive diverse da parte del cliente/CT.


## Setup<a name="setup"></a>

#### _Il progetto deve essere creato utilizzando l'archetipo Boost_

All'interno della cartella `frontend-src` sono presenti i file necessari per poter partire allo sviluppo si un sito backoffice con il tema inspinia.  
Di default viene creata la cartella webadmin per il backoffice.  

### Prerequisiti

`npm` versione 6.5.0+.  
Ide che permetta di creare dei webserver per i contenuti statici. Plugin utile per VS-Code: [Live Server](https://marketplace.visualstudio.com/items?itemName=ritwickdey.LiveServer). 


### Prima della build

Dalla cartella `frontend-src` eseguire il comando:  

```
$> npm ci
```

__Passaggio necessario anche se non è richiesto lo sviluppo di componenti React__.   
Creare, se non già presente, un file chiamato `config.local.js` nella cartella `frontend-src/src/webadmin/src/back-office/` con all'interno le configurazioni presenti nel file `config.example.js`.  

### Build

Per effettuare una build eseguire il comando:

```
$> npm run build:<env> -- --env.projects=<projects> --env.platforms=<platforms> --env.watch

``` 
dove:

- `<env>`: Sostituire con la stringa relativa all'ambiente da buildare. Per lo sviluppo usare `local`.
- `<projects>`: I progetti da buildare separati da `,`. Se non specificati verranno buildati tutti i progetti specificati nel file `package.json`.
- `<platforms>`: Le piattaforme da buildare per ogni progetto. Di default vengono prese quelle specificate nel file `package.json`.
- `env.watch`: Solo se necessario mantenere la build attiva ad ogni modifica dei file. 

## Linee guida sviluppo<a name="linee-guida"></a>

### Sviluppo dei samples

Durante lo sviluppo dei samples è necessario eseguire il comando: 

```
$> npm run watchbuild 
```
a cui è possibile concatenare i parametri `--env.` del comando build.   


Per facilitare lo sviluppo dei samples e ridurre la ripetizione del codice, è consigliato sviluppare utilizzando il linguaggio di templating [Handlebars](https://handlebarsjs.com/).  

Nella cartella `frontend-src/src/<project_name>/samples` sono presenti due cartelle:  

- `pages`: contiene i file che in fase di build vengono trasformati in file `.html` e messi nella folder di destinazione della build
- `partials`: contiene a sua volta due cartelle: `components` per contenre le varie componenti da riutilizzare e `layouts` per definire le parti comuni delle pagine. Documentazione dei partials di handelbars [qui](https://handlebarsjs.com/partials).

### Stile 

Per la parte relativa agli stili, durante la procedura di build vengono "compilati" i file .scss  e i file .css vengono minifizzati e concatenati.  
Nella cartella `frontend-src/src/<project>/style/<platform>/scss` sono presenti tutti i file .scss necessari.  
Per aggiungere un file css già compilato e/o minifizzato bisogna aggiungerlo all'elenco di file presenti in `frontend-src/webpack-modules/scss-webpack.config.js`. I file verranno compilati e concatenati seguendo l'ordine specificato.   
_NB: non è necessario aggiungere il file `index.scss` all'elenco._


### Componenti Javascript/JQuery

Anche per la parte di javascript i file vengono concatenati e minifizzati in un file solo.  
Per lo sviluppo di plugin o funzioni utils è necessario aggiungere il file da includere all'interno dell'elenco di file presente nel file `frontend-src/webpack-modules/plugins-webpack.config.js`.  
Sono già inclusi all'interno del progetto due file javascript nella cartella `frontend-src/src/<project>/javascripts`:

- `functions.js`: contiene alcune funzioni utili che possono essere utilizzate ovunque.
- `utils.js`: contiene codice che viene eseguito su ogni pagina del sito. Utile per inserire plugin e codice comune. Es chosen select o wyswyg editor.

è utile inserire in questa cartella anche i plugin jQuery custom del progetto.


### Sviluppo componenti ReactJS

Per lo sviluppo in ReactJS delle componenti necessarie, eseguire il comando:

```
$> npm run start:<env>
```

Utilizzando questo comando i file non saranno copiati nella cartella di destinazione, tutto verrà eseguito utilizzando `webpack-dev-server`.  


 
 


