package it.vidiemme.school.webapi.web.error;

public class ApiFieldError {
	private String field;
	private String code;
	private Object rejectedValue;
	private Object[] errorParams;

	public ApiFieldError(String field, String code, Object rejectedValue, Object[] errorParams) {
		super();
		this.field = field;
		this.code = code;
		this.rejectedValue = rejectedValue;
		this.errorParams = errorParams;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Object getRejectedValue() {
		return rejectedValue;
	}

	public void setRejectedValue(Object rejectedValue) {
		this.rejectedValue = rejectedValue;
	}

    public Object[] getErrorParams() {
        return errorParams;
    }

    public void setErrorParams(Object[] errorParams) {
        this.errorParams = errorParams;
    }
}
