import BaseService from './base-service';
import AuthenticationRepository from '../repositories/authentication';
import CustomErrorWithData from '../utilities/custom-error-with-data';

/**
 * Service per gestire l'autenticazione
 */
class AuthenticationService extends BaseService {
    constructor() {
        super();
        this.authenticationRepository = new AuthenticationRepository();
    }

    /**
     * Service per la gestione della login
     *
     * @param {string} username Username dell'utente
     * @param {string} password Password dell'utente
     * @returns {Promise}
     */
    signin(username, password) {
        return this.authenticationRepository.signin(username, password)
            .then(data => this.authenticationRepository.storeAuthentication(data.data.token).then(() => data))
            // @TODO: gestire qui eventuali errori per assicurare la transazionalità dell'operazione
            .catch((error) => {
                throw new CustomErrorWithData(error.data, 'Something went wrong.');
            });
    }

    /**
     * Service per la gestione della logout
     *
     * @returns {Promise}
     */
    signout() {
        return this.authenticationRepository.signout()
            .then(data => this.authenticationRepository.removeAuthentication().then(() => data))
            // @TODO: gestire qui eventuali errori per assicurare la transazionalità dell'operazione
            .catch((error) => {
                throw new CustomErrorWithData(error.data, 'Something went wrong.');
            });
    }

    /**
     * Restituisce se l'utente è loggato o meno.
     *
     * @returns {boolean} true se l'utente è loggato, false altrimenti
     */
    isLoggedIn() {
        return this.authenticationRepository.getAuthentication()
            .then(data => !!data)
            .catch((error) => {
                throw new CustomErrorWithData(error.data, 'Something went wrong.');
            });
    }
}

export default AuthenticationService;
