/*
 * and open the template in the editor.
 */
package it.vidiemme.school.webadmin.web.validator;


import javax.validation.groups.Default;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.SmartValidator;

import it.vidiemme.school.business.model.User;
import it.vidiemme.school.business.security.UserPrincipal;
import it.vidiemme.school.business.validator.ValidationConstants;
import it.vidiemme.school.webadmin.web.form.ChangePasswordForm;

/**
 * Change password form validator
 */
@Component
public class ChangePasswordValidator implements SmartValidator {

	@Autowired
	private PasswordEncoder passwordEncoder;
    
	@Override
	public boolean supports(Class<?> clazz) {
		return ChangePasswordForm.class.equals(clazz);
	}

    @Override
    public void validate(Object target, Errors errors, Object... validationHintsArray) {
        applyValidation(target, errors, validationHintsArray);
    }

    @Override
    public void validate(Object target, Errors errors) {
        applyValidation(target, errors, Default.class);
    }
    
    public void applyValidation(Object target, Errors errors, Object... validationHintsArray) {
        ChangePasswordForm cp = (ChangePasswordForm) target;

        if (!cp.getOldPassword().isEmpty()) {
            UserPrincipal principal = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
            User loggedUser = principal.getUser();
            if (!passwordEncoder.matches(cp.getOldPassword() ,loggedUser.getPassword())) {
                errors.rejectValue("oldPassword", ValidationConstants.FIELD_INVALID);
            }
        }

        if (!cp.getNewPassword().isEmpty()) {
            if (cp.getOldPassword().equals(cp.getNewPassword())) {
                errors.rejectValue("newPassword", ValidationConstants.FIELD_NOT_DIFFERENT);
            }
        }
        if (!cp.getConfirmNewPassword().isEmpty() && !cp.getConfirmNewPassword().equals(cp.getNewPassword())) {
            errors.rejectValue("confirmNewPassword", ValidationConstants.FIELDS_MISMATCH);
        }
    }
}
