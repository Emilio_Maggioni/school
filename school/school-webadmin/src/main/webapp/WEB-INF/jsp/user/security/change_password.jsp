<%@ include file="/includes/taglibs.jsp"%>
<head>
    <title><spring:message code="user.changepassword" /> <spring:message code="project.brand"/></title>
</head>
<body>
    <div class="middle-box text-center loginscreen animated fadeInDown">
        <spring:hasBindErrors name="changePasswordForm">
            <jsp:include page="/WEB-INF/jsp/error_messages.jsp" />
        </spring:hasBindErrors>
        <h2><spring:message code="user.changepassword" /></h2>
        <form:form  class="m-t" method="post" action="${ctx}/user/change-password" modelAttribute="changePasswordForm" >
            <div class="row">
                <spring:bind path="oldPassword">
                    <div class="form-group ${status.error ? 'has-error' : ''}">
                        <label class="control-label" for=oldPassword><spring:message code="user.oldPassword"/></label>
                        <form:password path="oldPassword" class="form-control" cssErrorClass="form-control is-invalid" required="required"/>
                        <form:errors path="oldPassword" cssClass="invalid-feedback" element="span"/>
                    </div>
                </spring:bind>
            </div>
            <div class="row">
                <spring:bind path="newPassword">
                    <div class="form-group ${status.error ? 'has-error' : ''}">
                        <label class="control-label" for="newPassword"><spring:message code="user.newPassword" /></label>
                        <form:password path="newPassword" class="form-control" cssErrorClass="form-control is-invalid" required="required"/>
                        <form:errors path="newPassword" cssClass="invalid-feedback" element="span"/>
                    </div>
                </spring:bind>
            </div>
            <div class="row">
                <spring:bind path="confirmNewPassword">
                    <div class="form-group ${status.error ? 'has-error' : ''}">
                        <label class="control-label" for="confirmNewPassword"><spring:message code="user.verifyPassword" /></label>
                        <form:password path="confirmNewPassword" class="form-control" cssErrorClass="form-control is-invalid" required="required"/>
                        <form:errors path="confirmNewPassword" cssClass="invalid-feedback" element="span"/>
                    </div>
                </spring:bind>
            </div>
            <div class="p-h-md">
                <button type="submit" id="save-user"  class="btn btn-primary block full-width" ><spring:message code="save"/></button>
            </div>
        </form:form>
    </div>
</body>

