const pathConfig = require('./path-webpack.config');
const path = require('path');
const vendorStyles = require('./scss-webpack.config');

module.exports = (PROFILE, project, platform) => {
    return {
        [platform]: vendorStyles(project, platform).concat([
            path.resolve('.',
                'src',
                project,
                'style',
                platform,
                'scss',
                'index.scss'),
            path.resolve('.',
                'src',
                project,
                'src',
                platform,
                'index.js'
            )
        ]),
        [platform + '-config']: path.resolve(
            '.',
            'src',
            project,
            'src',
            platform,
            'config.' + PROFILE + '.js')
    }
};
