package it.vidiemme.school.webadmin.web.validator;

import javax.validation.groups.Default;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.SmartValidator;

import it.vidiemme.school.business.model.User;
import it.vidiemme.school.business.service.UserService;
import it.vidiemme.school.business.validator.ValidationConstants;
import it.vidiemme.school.webadmin.web.form.NewPasswordForm;

/**
 * Set new password form validator
 */
@Component
public class NewPasswordValidator implements SmartValidator {

	@Autowired
	private UserService userService;
	
	@Autowired
	private PasswordEncoder passwordEncoder;

	@Override
	public boolean supports(Class<?> clazz) {
		return NewPasswordForm.class.equals(clazz);
	}

    @Override
    public void validate(Object target, Errors errors, Object... validationHintsArray) {
        applyValidation(target, errors, validationHintsArray);
    }

    @Override
    public void validate(Object target, Errors errors) {
        applyValidation(target, errors, Default.class);
    }
    
    public void applyValidation(Object target, Errors errors, Object... validationHintsArray) {
        NewPasswordForm passwordForm = (NewPasswordForm) target;

        String newPassword = passwordForm.getNewPassword();
        if (newPassword != null) {

            // check that "new password" field matches "confirm new password" field
            String confirmPassword = passwordForm.getConfirmNewPassword();
            if (confirmPassword != null && !newPassword.equals(confirmPassword)) {
                errors.rejectValue("confirmNewPassword", ValidationConstants.FIELDS_MISMATCH);
            }

            // check if the token is valid and if the user is not deleted 
            User user = userService.getByToken(passwordForm.getToken());
            if (user == null) {
                errors.reject("user.setnewpassword.unauthorized");
            } else {
                if (user.getIsDeleted()) {
                    errors.reject("user.setnewpassword.unauthorized");
                }
            }

            // check if the new password is different from the actual password
            if (user != null && passwordEncoder.matches(newPassword, user.getPassword())) {
                errors.rejectValue("newPassword", ValidationConstants.FIELD_NOT_DIFFERENT);
            }
        }
    }    
}
