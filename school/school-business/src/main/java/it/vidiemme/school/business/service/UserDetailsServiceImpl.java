package it.vidiemme.school.business.service;

import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import it.vidiemme.school.business.dao.UserMapper;
import it.vidiemme.school.business.model.User;
import it.vidiemme.school.business.security.UserPrincipal;

@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserMapper userMapper;
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    	userMapper.selectByExample(null);
    	
        User user = userMapper.loadByUsernameWithRolesAndPrivileges(username);
        if (user == null) {
        	logger.info(String.format("User %s not found", username));
            throw new UsernameNotFoundException(username);
        }
        return new UserPrincipal(user, getUserGrantedAuthorities(user));
    }
    
    private Collection<GrantedAuthority> getUserGrantedAuthorities(User user) {
        Set<GrantedAuthority> authorities = new HashSet<>();
        user.getPrivileges().stream().map(p -> new SimpleGrantedAuthority(p)).forEach(authorities::add);        
        user.getRoles().stream().map(r -> new SimpleGrantedAuthority(r.getName())).forEach(authorities::add);
        return authorities;
    }
}
