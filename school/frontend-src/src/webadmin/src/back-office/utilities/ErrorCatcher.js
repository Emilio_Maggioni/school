import React from 'react';
import PropTypes from 'prop-types';

class ErrorCatcher extends React.Component {
    static get propTypes() {
        return {
            children: PropTypes.oneOfType([
                PropTypes.element,
                PropTypes.arrayOf(PropTypes.element),
                PropTypes.string,
            ]).isRequired,
        };
    }

    // static getDerivedStateFromError(error) {
    //     // Aggiorno lo stato in modo che il prossimo render visualizzi la UI di ripiego.
    //     return {
    //         hasError: true,
    //     };
    // }

    componentDidCatch(error, info) {
        console.error(error, info, this);
    }

    render() {
        return this.props.children;
    }
}

export default ErrorCatcher;
