import React, { Fragment } from 'react';
import { HashRouter as Router, Switch, Route } from 'react-router-dom';

import { Authenticated, Guest } from '../middleware';

import Login from '../containers/Login';

export default function Routes() {
    return (<Router>
        <Switch>
            <Route exact path='/route-2' render={() => 'route-2'} />
            <Guest exact path='/login' component={Login} />
            <Authenticated>
                <Fragment>
                    <Route exact path='/route-1' render={() => 'route-1'} />
                    <Route exact path='/' render={() => 'route-0'} />
                </Fragment>
            </Authenticated>
        </Switch>
    </Router>);
}
