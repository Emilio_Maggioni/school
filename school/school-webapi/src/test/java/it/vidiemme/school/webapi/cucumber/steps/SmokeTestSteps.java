package it.vidiemme.school.webapi.cucumber.steps;

import static org.junit.Assert.assertTrue;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.Map;

import org.apache.commons.lang.StringUtils;
import org.apache.http.client.utils.URIBuilder;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.web.WebAppConfiguration;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import it.vidiemme.school.webapi.config.CucumberConfig;
import it.vidiemme.school.webapi.config.TestAppConfig;
import it.vidiemme.school.webapi.config.TestRepositoryConfig;
import it.vidiemme.school.webapi.config.WebConfig;
import it.vidiemme.school.webapi.cucumber.worlds.SmokeWorld;

@WebAppConfiguration
@TestPropertySource(locations = {
		"classpath:properties/app.properties", 
		"classpath:properties/mail.properties", 
		"classpath:properties/jdbc.properties"
		})
@ContextConfiguration(classes = {CucumberConfig.class, TestAppConfig.class, TestRepositoryConfig.class, WebConfig.class})
public class SmokeTestSteps {

    @Autowired
    private SmokeWorld world;

    @Given("^A base URL (.*)$")
    public void setBaseUrl(String baseUrl) throws URISyntaxException {
        world.basePath = baseUrl;
        world.uriBuilder = new URIBuilder(baseUrl);
    }

    @When("^Call the GET (.*) url$")
    public void callGetPathUrl(String path) {
        final String originalPath = world.uriBuilder.getPath();
        world.uriBuilder.setPath(originalPath + path);
    }

    @Then("^I get api version (.*)$")
    @SuppressWarnings("unchecked")
    public void verifyApiVersion(String version) throws MalformedURLException, URISyntaxException {
        ResponseEntity<?> response = callRestUrl(world.uriBuilder, HttpMethod.GET, null);
        assertTrue(response != null && response.getBody() != null);
        Map<String, String> body = (Map<String, String>) response.getBody();
        assertTrue(body.containsKey("api-version") && body.containsKey("build") && body.get("api-version").equals(version));
    }

    @SuppressWarnings("rawtypes")
    @Given("^login credential: username (.*) and password (.*)$")
    public void userLoginGivenCredentials(String username, String password) throws URISyntaxException, MalformedURLException, JSONException {
        URIBuilder uri = new URIBuilder(world.basePath);
        uri.setPath(uri.getPath() + "/login");
        JSONObject json = new JSONObject();
        json.put("username", username);
        json.put("password", password);
        ResponseEntity<?> loginResponse = callRestUrl(uri, HttpMethod.POST, json.toString());
        JSONObject o = new JSONObject((Map) loginResponse.getBody());
        world.token = o.get("token").toString();
    }

    private ResponseEntity<?> callRestUrl(URIBuilder uriBuilder, HttpMethod method, String postJson) throws MalformedURLException, URISyntaxException {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl(uriBuilder.build().toURL().toString());
        RestTemplate restTemplate = new RestTemplate();
        HttpHeaders headers = new HttpHeaders();
        headers.set("Content-type", MediaType.APPLICATION_JSON_VALUE);
        headers.set("Accept", MediaType.APPLICATION_JSON_VALUE);
        if (StringUtils.isNotEmpty(world.token))
            headers.set("Authorization", world.token);
        HttpEntity<String> entity = new HttpEntity<>(postJson, headers);
        ResponseEntity<?> response = restTemplate.exchange(builder.toUriString(), method, entity, Object.class);
        return response;
    }
}
