package it.vidiemme.school.webapi.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import it.vidiemme.school.webapi.cucumber.worlds.EndToEndTestsWorld;
import it.vidiemme.school.webapi.cucumber.worlds.SmokeWorld;

/**
 * Create beans used by Cucumber to preserve state within each test class
 * @author flavio.ricci
 */
@Configuration
@IgnoreDuringComponentScan
public class CucumberConfig {

	@Bean
	public EndToEndTestsWorld endToEndTestsWorld() {
		return new EndToEndTestsWorld();
	}
    
	@Bean
	public SmokeWorld smokeWorld() {
		return new SmokeWorld();
	}
}
