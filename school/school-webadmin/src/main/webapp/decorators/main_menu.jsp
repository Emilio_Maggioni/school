<%@ include file="/includes/taglibs.jsp"%>
<head>
    <sitemesh:write property='head'/>
    <title><sitemesh:write property='title'/></title>
</head>
<body>      
    <sitemesh:write property='body'/>   
</body>
<content tag="mainmenu">

  <security:authorize access="hasAuthority('USERS_MANAGE')">
      <li class="${currentMenuItem == 'manage-users' ? 'active':''}">
            <a href="${ctx}/user"> <i class="fa fa-user"></i> <span class="nav-label"><spring:message code="user.manage"/></span></a>
      </li>
  </security:authorize>
  <security:authorize access="hasAuthority('EXAMPLE_SECTION_VIEW')">
      <li class="${currentMenuItem == 'example-section' ? 'active':''}">
        <a href="${ctx}/example-section"> <i class="fa fa-diamond"></i> <span class="nav-label"><spring:message code="example.section"/></span></a>
      </li>
  </security:authorize>
  <li class="${currentMenuItem == 'dashboards' ? 'active':''}"><a href="index.html"> <i class="fa fa-th-large"></i> <span class="nav-label">Dashboards</span> <span class="fa arrow"></span></a>
	<ul class="nav nav-second-level">
	    <li><a href="#">Dashboard v.1</a></li>
	    <li><a href="#">Dashboard v.2</a></li>
	    <li><a href="#">Dashboard v.3</a></li>
	    <li><a href="#">Dashboard v.4</a></li>
	    <li><a href="#">Dashboard v.5 </a></li>
	</ul>
  </li>
</content>

<content tag="localscript">
    <sitemesh:write property="page.localscript"></sitemesh:write>
</content>