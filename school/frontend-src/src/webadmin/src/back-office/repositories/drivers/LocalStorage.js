import CustomErrorWithData from '../../utilities/custom-error-with-data';

/**
 * Gestisce l'accesso a LocalStorage in maniere asincrona
 *
 * @param {string} prefix Prefisso con cui archiviare i dati
 */
class LocalStorage {
    constructor(prefix = '') {
        if (prefix.length > 0) {
            this.prefix = `${prefix}_`;
        } else {
            this.prefix = '';
        }
    }

    /**
     * Archivia il dato
     *
     * @param {string} key Chiave con cui archiviare value
     * @param {*} value Valore da archiviare
     * @returns {Promise}
     */
    store(key, value = null) {
        return new Promise((resolve, reject) => {
            try {
                window.localStorage.setItem(`${this.prefix}_${key}`, JSON.stringify(value));
                resolve();
            } catch (error) {
                reject(new CustomErrorWithData(error));
            }
        });
    }

    /**
     * Recupera un dato precedentemente archiviato
     *
     * @param {string} key Chiave con cui recuperare il dato
     * @returns {Promise}
     */
    get(key) {
        return new Promise((resolve, reject) => {
            try {
                resolve(JSON.parse(window.localStorage.getItem(`${this.prefix}_${key}`)));
            } catch (error) {
                reject(new CustomErrorWithData(error));
            }
        });
    }

    /**
     * Elimina un dato precedentemente archiviato
     *
     * @param {string} key Chiave tramite cui eliminare il dato
     * @returns {Promise}
     */
    remove(key) {
        return new Promise((resolve, reject) => {
            try {
                window.localStorage.removeItem(`${this.prefix}_${key}`);
                resolve();
            } catch (error) {
                reject(new CustomErrorWithData(error));
            }
        });
    }

    /**
     * Svuota l'archivio
     *
     * @returns {Promise}
     */
    static clear() {
        return new Promise((resolve, reject) => {
            try {
                window.localStorage.clear();
                resolve();
            } catch (error) {
                reject(new CustomErrorWithData(error));
            }
        });
    }
}

export default LocalStorage;
