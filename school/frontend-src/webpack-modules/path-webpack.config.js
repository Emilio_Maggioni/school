const path = require('path');

/**
 * Cartella root del progetto npm
 *
 * @ignore
 * @type {string}
 */
const SOURCE_DIR = path.resolve(__dirname, '..');

/**
 * Cartella root dell'applicazione react
 *
 * @ignore
 * @type {string}
 */
const APP_DIR = path.resolve(__dirname, '..', 'src');

/**
 * Cartella di destinazione dei file processati da webpack
 *
 * @ignore
 * @type {string}
 */
const BUILD_DIR = path.resolve(__dirname, '..');

module.exports = {
    SOURCE_DIR: SOURCE_DIR,
    BUILD_DIR: BUILD_DIR,
    APP_DIR: APP_DIR,
};
