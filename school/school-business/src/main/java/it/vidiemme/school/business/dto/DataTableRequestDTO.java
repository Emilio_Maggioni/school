package it.vidiemme.school.business.dto;

import java.util.List;
import java.util.Map;

public class DataTableRequestDTO<T> {
    
    private Integer draw;
    private Integer length; //Number of records that the table can display in the current draw
    private Integer start; //Paging first record indicator (0 is the first record)
    private List<Map<String, Object>> order;
    private T filter;
    
    public Integer getDraw() {
        return draw;
    }
    public void setDraw(Integer draw) {
        this.draw = draw;
    }
    public Integer getLength() {
        return length;
    }
    public void setLength(Integer length) {
        this.length = length;
    }
    public Integer getStart() {
        return start;
    }
    public void setStart(Integer start) {
        this.start = start;
    }
    public List<Map<String, Object>> getOrder() {
        return order;
    }
    public void setOrder(List<Map<String, Object>> order) {
        this.order = order;
    }
    public T getFilter() {
        return filter;
    }
    public void setFilter(T filter) {
        this.filter = filter;
    }
    
    
}
