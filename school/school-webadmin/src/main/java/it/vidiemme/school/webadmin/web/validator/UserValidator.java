package it.vidiemme.school.webadmin.web.validator;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import javax.validation.groups.Default;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;
import org.springframework.validation.SmartValidator;

import it.vidiemme.school.business.model.User;
import it.vidiemme.school.business.service.UserService;
import it.vidiemme.school.business.validator.ValidationConstants;
import it.vidiemme.school.business.validator.ValidationGroups.OnCreate;
import it.vidiemme.school.business.validator.ValidationGroups.OnUpdate;
import it.vidiemme.school.webadmin.web.form.UserForm;

/**
 * New user / edit user form validator
 */
@Component
public class UserValidator implements SmartValidator {

    private final Logger LOG = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private UserService userService;

    @Override
    public boolean supports(Class<?> clazz) {
        return UserForm.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors, Object... validationHintsArray) {
        applyValidation(target, errors, validationHintsArray);
    }

    @Override
    public void validate(Object target, Errors errors) {
        applyValidation(target, errors, Default.class);
    }
    
    public void applyValidation(Object target, Errors errors, Object... validationHintsArray) {
        UserForm userForm = (UserForm) target;
        
        Set<Object> validationHints = new HashSet<Object>(Arrays.asList(validationHintsArray));

        if (!errors.hasErrors()) {
            User existingUser = userService.getByUsername(userForm.getUsername());
            if (validationHints.contains(OnCreate.class)) {
                if (existingUser != null) {
                    errors.rejectValue("username", ValidationConstants.FIELD_ALREADY_USED);
                }
            } else if (validationHints.contains(OnUpdate.class)) {
                if (existingUser != null && !existingUser.getId().equals(userForm.getId())) {
                    errors.rejectValue("username", ValidationConstants.FIELD_ALREADY_USED);
                }
            }
        }
    }    
}
