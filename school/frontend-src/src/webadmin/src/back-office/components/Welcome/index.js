import React, { Component } from 'react';

const generateNewRandomName = () => {
    try {
        const randomNumber = Math.random();
        if (randomNumber > 0.3) {
            return randomNumber.toString(36).substring(7);
        }
        throw new Error('Example error generating a new random name.');
    } catch (error) {
        console.error(error);
        toastr.error(error.message, i18next.t('dialogs.error'));
    }
    return '-';
};

class Welcome extends Component {
    constructor(props) {
        super(props);

        if (!window.user) {
            console.error('Component Welcome not initialized by server-side.'); // eslint-disable-line
            this.state = {
                user: {},
            };
            return;
        }
        this.state = {
            user: window.user,
        };

        this.buttonClick = this.buttonClick.bind(this);
    }


    buttonClick() {
        this.setState({
            user: {
                name: generateNewRandomName(),
            },
        });
    }

    render() {
        return <div className="row m-0 border-bottom white-bg dashboard-header">
            <div className="col-md-5">
                <h2>{i18next.t('welcome.welcome', {
                    name: this.state.user.name,
                })}</h2>
            </div>
            <div className="col-md-3">
                <button type="button"
                    className="btn btn-w-m btn-primary"
                    onClick={this.buttonClick}
                >
                    {i18next.t('welcome.randomizeName')}
                </button>
            </div>
        </div>;
    }
}

export default Welcome;
