package it.vidiemme.school.webapi.config;

import javax.sql.DataSource;

import org.apache.commons.dbcp2.BasicDataSource;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.PropertySource;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.Resource;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;
import org.springframework.jdbc.datasource.DataSourceTransactionManager;
import org.springframework.jdbc.datasource.init.DatabasePopulator;
import org.springframework.jdbc.datasource.init.DatabasePopulatorUtils;
import org.springframework.jdbc.datasource.init.ResourceDatabasePopulator;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * H2 in memory test database
 */
@Configuration
@EnableTransactionManagement
@PropertySource({ "classpath:properties/jdbc.properties" })
@ComponentScan(basePackages = { "it.vidiemme.school.business" })
@MapperScan("it.vidiemme.school.business.dao")
public class TestRepositoryConfig {

    @Value("${jdbc.driverClassName}")
    private String jdbcDriverClassName;
    @Value("${jdbc.url}") 
    private String jdbcUrl;
    @Value("${jdbc.username}")
    private String jdbcUsername;
    @Value("${jdbc.password}")
    private String jdbcPassword;
    

    /** The bean manages the connections to the physical data source, reading the configuration from the jdbc.properties file.
     * @return the DataSource bean
     */
    @Bean
    public DataSource dataSource() {

        BasicDataSource ds = new BasicDataSource();
        ds.setDriverClassName(jdbcDriverClassName);
        ds.setUrl(jdbcUrl);
        ds.setUsername(jdbcUsername);
        ds.setPassword(jdbcPassword);
        ds.setInitialSize(10);
        ds.setMaxTotal(25);
        ds.setMaxWaitMillis(5000);
        ds.setPoolPreparedStatements(true);
        ds.setDefaultAutoCommit(Boolean.FALSE);
        ds.setRemoveAbandonedTimeout(60);
        
        //populate in-memory H2 database used only in text context (see jdbc.properties in src/test/resources/properties)
        Resource initSchema = new ClassPathResource("h2-bootstrap/schema-h2.sql");
		Resource initData = new ClassPathResource("h2-bootstrap/data-h2.sql");
		DatabasePopulator databasePopulator = new ResourceDatabasePopulator(initSchema, initData);
		DatabasePopulatorUtils.execute(databasePopulator, ds);
        
        return ds;
    }

    /** The bean handles the database transactions
     * @param ds the DataSource
     * @return the DataSourceTransactionManager bean
     */
    @Bean
    public PlatformTransactionManager transactionManager(DataSource ds) {
        return new DataSourceTransactionManager(ds);
    }

    /** The bean handles the MyBatis sessions creation.
     * The location of MyBatis mapper xml files (/resources/sqlmaps/) is specified as a sessionFactory configuration parameter.
     * @param ds the DataSource
     * @return the SqlSessionFactoryBean bean
     * @throws Exception
     */
    @Bean
    public SqlSessionFactoryBean sqlSessionFactory(DataSource ds) throws Exception {
        SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
        sessionFactory.setMapperLocations(new PathMatchingResourcePatternResolver().getResources("classpath:sqlmaps/*.xml"));
        sessionFactory.setDataSource(ds);
        org.apache.ibatis.session.Configuration configuration = new org.apache.ibatis.session.Configuration();
        configuration.setCallSettersOnNulls(true);
        sessionFactory.setConfiguration(configuration);
        return sessionFactory;
    }
}
