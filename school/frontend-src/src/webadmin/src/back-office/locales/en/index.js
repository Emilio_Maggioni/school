import commons from './commons';
import login from './login';

const en = {
    commons,
    login,
};

export default en;
