package it.vidiemme.school.business.utils;

import java.net.URI;

import javax.servlet.http.HttpServletRequest;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

public class Utils {
    
    public static String getApplicationAbsoluteUrl() {
        ServletRequestAttributes sra = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
        HttpServletRequest req = sra.getRequest();
        String serverPort = (req.getServerPort() == 80) ? "" : ":" + req.getServerPort();
        URI uri = URI.create(req.getScheme().concat("://").concat(req.getServerName()).concat(serverPort).concat(req.getContextPath()));
        return uri.toString();
    }

    public static String getApplicationContextPath() {
        ServletRequestAttributes sra = (ServletRequestAttributes)RequestContextHolder.getRequestAttributes();
        URI uri = URI.create(sra.getRequest().getServletContext().getContextPath());
        return uri.toString();
    }
}
