package it.vidiemme.school.webapi.web.controller;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.vidiemme.school.webapi.service.JwtTokenService;
import it.vidiemme.school.webapi.web.dto.JwtAuthenticationRequest;
import it.vidiemme.school.webapi.web.dto.JwtAuthenticationResponse;
import it.vidiemme.school.webapi.web.validator.LoginValidator;


/**
 * Controller to manage login related requests
 * @author flavio.ricci
 */
@RestController
@RequestMapping("${api.base.path}")
public class LoginController {

    private static final Logger logger = LoggerFactory.getLogger(LoginController.class);
    
    @Value("${jwt.header}")
    private String tokenHeader;
    
    @Autowired
    private LoginValidator loginValidator;

    @Autowired
    private JwtTokenService jwtTokenService;
    
    /**
     * Add validators 
     */
    @InitBinder("jwtAuthenticationRequest")
    public void setupUserBinder(WebDataBinder binder) {
        binder.addValidators(loginValidator);
    }

    /**
     * Logs in the user and creates the authentication token. 
     * The authentication token is stored into Authorization header and the JSON body
     */
    @PostMapping(value = "login", consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    public ResponseEntity<?> login(@RequestBody @Validated JwtAuthenticationRequest authenticationRequest, HttpServletResponse response) {
        logger.debug(String.format("Logging in user %s", authenticationRequest.getUsername()));
        JwtAuthenticationResponse loginResponse = jwtTokenService.login(authenticationRequest.getUsername(), authenticationRequest.getPassword());
        response.setHeader(tokenHeader, loginResponse.getToken()); 
        return ResponseEntity.status(HttpStatus.OK).body(loginResponse);
    }
}