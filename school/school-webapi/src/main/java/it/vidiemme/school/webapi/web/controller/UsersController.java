package it.vidiemme.school.webapi.web.controller;

import java.util.List;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.validation.constraints.NotEmpty;
import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import it.vidiemme.school.business.exception.ServiceException;
import it.vidiemme.school.business.model.User;
import it.vidiemme.school.business.service.UserService;
import it.vidiemme.school.business.validator.constraint.ExistingUserId;
import it.vidiemme.school.webapi.service.UsersEmailService;
import it.vidiemme.school.webapi.web.dto.ForgotPasswordDTO;
import it.vidiemme.school.webapi.web.dto.NewPasswordDTO;
import it.vidiemme.school.webapi.web.dto.UserDTO;

/**
 * Controller to manage users
 * 
 * @author flavio.ricci
 */
@RestController
@RequestMapping("${api.base.path}/users")
@Validated
public class UsersController {

    private static final Logger logger = LoggerFactory.getLogger(UsersController.class);

    @Autowired
    private UserService userService;
    @Autowired
    private UsersEmailService usersEmailService;

    /**
     * Creates a new user
     * 
     * @param userDTO the user details to store
     * @return the response entity
     */
    @PostMapping(consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    public ResponseEntity<?> create(@RequestBody @Validated UserDTO userDTO) {
        logger.debug(String.format("Creating user %s", userDTO));

        User user = userDTO.toUser();
        user = userService.addUser(user, userDTO.getRoles());
        userDTO.setId(user.getId());

        logger.info(String.format("Created user %s with id %d", userDTO.getUsername(), userDTO.getId()));
        return ResponseEntity.status(HttpStatus.CREATED).body(userDTO);
    }

    /**s
     * Retrieves user details by user id
     * 
     * @param id the user id
     * @return the response entity containing user details
     */
    @GetMapping(value = "/{id}", produces = MediaType.APPLICATION_JSON)
    public ResponseEntity<?> getUserDetailsById(@ExistingUserId @PathVariable Integer id) {
        logger.debug(String.format("Getting user details of user %d", id));

        User user = userService.getById(id, Boolean.TRUE);
        UserDTO userDTO = UserDTO.fromUser(user);

        return ResponseEntity.status(HttpStatus.OK).body(userDTO);
    }

    /**
     * Deletes a user by user id
     * Note: it doesn't phisically delete a user from DB, but sets IS_DELETED flag to 1
     * 
     * @param id the user id
     * @return the response entity
     */
    @DeleteMapping(value = "/{id}")
    public ResponseEntity<?> deleteUserById(@ExistingUserId @PathVariable Integer id) {
        logger.debug(String.format("Deleting user %d", id));

        userService.delete(id);

        logger.info(String.format("User %d deleted", id));
        return ResponseEntity.status(HttpStatus.NO_CONTENT).build();
    }

    /**
     * Updates a user by user id and passed-in DTO details
     * 
     * @param id the user id to update
     * @param userDTO the user details to save
     * @return the response entity
     */
    @PutMapping(value = "/{id}", consumes = MediaType.APPLICATION_JSON)
    public ResponseEntity<?> updateUserById(@ExistingUserId @PathVariable @NotEmpty Integer id, @RequestBody @Validated UserDTO userDTO) {
        logger.debug(String.format("Updating user %d with values %s", id, userDTO));

        User user = userDTO.toUser();
        user.setId(id);
        userService.update(user, userDTO.getRoles());

        logger.info(String.format("User %d updated", id));
        return ResponseEntity.status(HttpStatus.OK).body(userDTO);
    }

    /**
     * Request the user password reset
     * 
     * @param forgotPasswordDTO the user details (email)
     * @return the response entity
     */
    @PutMapping(value = "/reset-password", consumes = MediaType.APPLICATION_JSON)
    public ResponseEntity<?> resetUserPassword(@RequestBody @Validated ForgotPasswordDTO forgotPasswordDTO, HttpServletRequest req) {
        logger.debug(String.format("Requesting reset password for user %s", forgotPasswordDTO.getEmail()));
        try {
            usersEmailService.sendResetPasswordMail(forgotPasswordDTO.getEmail(), req);
            logger.info(String.format("Reset password email sent for user %s", forgotPasswordDTO.getEmail()));
        } catch (ServiceException e) {
            logger.error("Error sending email during forgot password to" + forgotPasswordDTO.getEmail(), e);
        }
        return ResponseEntity.status(HttpStatus.OK).body(null);
    }

    /**
     * Set the user new password
     * 
     * @param newPasswordDTO the DTO containing the user token and the new password
     * @return the response entity
     */
    @PutMapping(value = "/set-new-password", consumes = MediaType.APPLICATION_JSON)
    public ResponseEntity<?> resetUserPassword(@RequestBody @Validated NewPasswordDTO newPasswordDTO, HttpServletRequest req) {
        logger.debug(String.format("Setting new password for user %s", newPasswordDTO.getEmail()));

        userService.setNewPassword(newPasswordDTO.getToken(), newPasswordDTO.getNewPassword());

        logger.info(String.format("New password set for user %s", newPasswordDTO.getEmail()));
        return ResponseEntity.status(HttpStatus.OK).body(null);
    }

    /**
     * Retrieves all the registered users
     * 
     * @return the response entity containing user details
     */
    @GetMapping(produces = MediaType.APPLICATION_JSON)
    public ResponseEntity<?> getAllUsers() {
        logger.debug("Getting user list");

        List<User> userList = userService.getAll();
        List<UserDTO> userDTOList = userList.stream().map(u -> UserDTO.fromUser(u)).collect(Collectors.toList());

        logger.debug(String.format("Retrieved %d users", userList.size()));
        return ResponseEntity.status(HttpStatus.OK).body(userDTOList);
    }
}