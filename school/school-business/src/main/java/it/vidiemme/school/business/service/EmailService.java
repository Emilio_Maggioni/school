package it.vidiemme.school.business.service;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.core.io.ClassPathResource;
import org.springframework.core.io.InputStreamSource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.context.Context;

@Service
public class EmailService {

    @Autowired
    private JavaMailSender mailSender;
    @Autowired
    private Environment env;
    @Autowired
    private TemplateEngine templateEngine;

    private final Logger log = LoggerFactory.getLogger(this.getClass());
    
    public void sendEmailWithTemplate(String[] tos, String subject, String body, Map<String, InputStreamSource> attachments) throws MessagingException, IOException {
        String logoResourceName = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + "_logo";
        String processedEmailBody = processEmailBody(body, logoResourceName);
        
        sendEmail(env.getProperty("mail.default.from"), tos, subject, processedEmailBody, attachments, Collections.singletonMap(logoResourceName, new ClassPathResource("/templates/img/email_logo.png")));
    }

    /** Sends an email to a list of recipients
     * @param tos the recipients
     * @param subject the email subject
     * @param body the email body
     * @param attachments the list of file attachments
     * @param inlineElements the list inlineElements (like images) to be included in the email
     * @throws MessagingException
     * @throws IOException
     */
    public void sendEmail(String from, String[] tos, String subject, String body, Map<String, InputStreamSource> attachments, Map<String, ClassPathResource> inlineElements) throws MessagingException, IOException {

        MimeMessage mimeMessage = mailSender.createMimeMessage();
        MimeMessageHelper messageHelper = new MimeMessageHelper(mimeMessage, true, "UTF-8");
        messageHelper.setFrom(from);
        messageHelper.setSubject(subject);
        messageHelper.setText(body, true);
        
        if (inlineElements != null) inlineElements.entrySet().forEach(e -> {
            try {
                messageHelper.addInline(e.getKey(), e.getValue());
            } catch (MessagingException ex) {
                log.error(ex.getMessage(), ex);
            }
        });
        
        if (attachments != null) attachments.entrySet().forEach(e -> {
            try {
                messageHelper.addAttachment(e.getKey(), e.getValue());
            } catch (MessagingException ex) {
                log.error(ex.getMessage(), ex);
            }
        });
        
        messageHelper.setTo(tos);
        messageHelper.setSentDate(new Date());
        mailSender.send(messageHelper.getMimeMessage());
    }

    /** Decorates the email body with the application html email template
     * @param body the email body
     * @param logoResourceName the resource name of the logo to be inserted in the header of the email
     * @return the processed email body
     */
    private String processEmailBody(String body, String logoResourceName) {

        Map<String, Object> templateModel = new HashMap<String, Object>();
        templateModel.put("body", body);
        templateModel.put("logo", logoResourceName);

        final Context ctx = new Context(Locale.ITALY);
        ctx.setVariables(templateModel);

        return templateEngine.process("email_template.html", ctx);
    }
}
