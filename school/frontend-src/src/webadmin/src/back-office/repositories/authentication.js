import BaseRepository from './base-repository';
import JSONRequest from './drivers/JSONRequest';
import LocalStorage from './drivers/LocalStorage';

const AUTHENTICATION_KEY = 'authentication_token';

/**
 * Classe che gestisce l'accesso al dato per l'autenticazione dell'utente
 *
 * @param {string} uri URI da concatenare al `baseURL` dichiarato nelle configurazioni
 */
class AuthenticationRepository extends BaseRepository {
    constructor(uri = '') {
        super(uri);

        this.localStorage = new LocalStorage(this.localStoragePrefix);
    }

    /**
     * Restituisce i dati per l'autenticazione.
     *
     * @returns {Promise}
     */
    getAuthentication() {
        return this.localStorage.get(AUTHENTICATION_KEY);
    }

    /**
     * Contatta il server per effettuare la login
     *
     * @param {string} username
     * @param {string} password
     * @returns {Promise}
     */
    signin(username, password) {
        const request = new JSONRequest({
            method: 'POST',
            url: {
                url: `${this.URL}/login`,
            },
            json: {
                username,
                password,
            },
        });
        return request.send();
    }

    /**
     * Archivia i dati per l'autenticazione nello storage.
     *
     * @param {string} token Token di autenticazione
     * @returns {Promise}
     */
    storeAuthentication(token) {
        return this.localStorage.store(AUTHENTICATION_KEY, token);
    }

    /**
     * Contatta il server per effettuare la logout
     *
     * @returns {Promise}
     */
    signout() {
        const request = new JSONRequest({
            method: 'POST',
            url: {
                url: `${this.URL}/logout`,
            },
        });
        return request.send();
    }

    /**
     * Elimina i dati dell'autenticazione dallo storage
     *
     * @returns {Promise}
     */
    removeAuthentication() {
        return this.localStorage.remove(AUTHENTICATION_KEY);
    }
}

export default AuthenticationRepository;
