package it.vidiemme.school.webapi.web.controller;

import java.util.HashMap;
import java.util.Map;

import javax.ws.rs.core.MediaType;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.AuthenticationException;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * This route responds to the configured base path
 */
@RestController
@RequestMapping("${api.base.path}")
public class GenericController {

	private static final Logger logger = LoggerFactory.getLogger(GenericController.class);
	
	@Value("${api.version}")
    private String apiVersion;
	
	/**
     * Returns the API version and the build number
     */
    @GetMapping(value = "version", produces = MediaType.APPLICATION_JSON)
    public Map<String, String> getVersion() {
        Map<String, String> response = new HashMap<>();
        response.put("build", apiVersion);
        response.put("api-version", "v1");
        return response;
    }
    
    /**
     * Example of protected call (see the unprotected routes on SecurityConfig bean, configure method)
     */
    @GetMapping(value = "protected", consumes = MediaType.APPLICATION_JSON, produces = MediaType.APPLICATION_JSON)
    public ResponseEntity<?> protectedCall() throws AuthenticationException {
        logger.debug("Protected route");
        return ResponseEntity.status(HttpStatus.OK).build();
    }
    
}
