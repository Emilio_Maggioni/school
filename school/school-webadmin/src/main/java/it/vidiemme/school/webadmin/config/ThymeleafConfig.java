package it.vidiemme.school.webadmin.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.thymeleaf.TemplateEngine;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.spring5.templateresolver.SpringResourceTemplateResolver;
import org.thymeleaf.templatemode.TemplateMode;
import org.thymeleaf.templateresolver.ITemplateResolver;

/**
 * Configures Thymeleaf as the template engine to use for emails.
 */
@Configuration
public class ThymeleafConfig implements ApplicationContextAware {

    private ApplicationContext applicationContext;

    @Autowired
    private MessageSource messageSource;

    @Override
	public void setApplicationContext(ApplicationContext applicationContext) {
        this.applicationContext = applicationContext;
    }

    /** The bean manages the template engine configuration, in particular the email templates resolver and the localized messages source.
    * @return the TemplateEngine bean
    */
    @Bean
    public TemplateEngine templateEngine() {
        SpringTemplateEngine engine = new SpringTemplateEngine();
        engine.setEnableSpringELCompiler(true);
        engine.setTemplateResolver(emailTemplateResolver());
        engine.setTemplateEngineMessageSource(messageSource);
        return engine;
    }

    /** Configures the template resolver, used to resolve the templates for emails.
    * @return the ITemplateResolver object
    */
    private ITemplateResolver emailTemplateResolver() {
        SpringResourceTemplateResolver resolver = new SpringResourceTemplateResolver();
        resolver.setApplicationContext(applicationContext);
        resolver.setPrefix("classpath:/templates/");
        resolver.setSuffix(".html");
        resolver.getCheckExistence();
        resolver.setTemplateMode(TemplateMode.HTML);
        return resolver;
    }
}
