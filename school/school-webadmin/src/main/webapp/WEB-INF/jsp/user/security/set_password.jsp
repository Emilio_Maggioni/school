<%@ include file="/includes/taglibs.jsp"%>
<head>
    <title><spring:message code="user.setnewpassword" /> <spring:message code="project.brand"/></title>
</head>
<body>
    <form:form class="m-t"  method="post" modelAttribute="newPasswordForm">
        <form:errors path="*" cssClass="alert alert-danger" element="div"/>
        <div class="form-group text-left">
            <label class="control-label"><spring:message code="user.newPassword"/></label>
            <form:input path="newPassword" class="form-control" type="password" required="required"/>
        </div>
        <div class="form-group text-left">
            <label class="control-label"><spring:message code="user.verifyPassword"/></label>
            <form:input path="confirmNewPassword" class="form-control" type="password" required="required"/>
        </div>
        <div class="p-h-md">
            <button type="submit" class="btn btn-primary block full-width" ><spring:message code="confirm"/></button>
        </div>
    </form:form>
</body>



