<%@include file="/includes/taglibs.jsp"%>
<c:if test="${not empty msgKO}" var="testSV_KO">
	<div class="row">
	    <div class="col-md-12">
		    <div class="alert alert-success alert-dismissable ">
		    	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">
		     		<i class="fa fa-times"></i>
		   		</button>
		        ${msgKO}
		    </div>
	    </div>
    </div>
    <c:remove var="msgKO" scope="session" /> 
</c:if>
<c:if test="${not empty msgOk}" var="testSV_OK">
	<div class="row">
	    <div class="col-md-12">
		    <div class="alert alert-success alert-dismissable ">
		    	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">
			      <i class="fa fa-times"></i>
			   	</button>
		        ${msgOk}
		    </div>
	    </div> 
    </div>
    <c:remove var="msgOk" scope="session" />             
</c:if> 
<c:if test="${not empty msgWarn}">
	<div class="row">
	    <div class="col-md-12">
	       <div class="alert alert-warning alert-dismissable">
		    	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">
		      		<i class="fa fa-times"></i>
		   		</button>
		        ${msgWarn}
	        </div>
	    </div>   
	</div>
    <c:remove var="msgWarn" scope="session" />         
</c:if>
