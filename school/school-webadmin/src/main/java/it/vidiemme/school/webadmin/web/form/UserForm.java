package it.vidiemme.school.webadmin.web.form;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Positive;

import org.hibernate.validator.constraints.Length;

import it.vidiemme.school.business.model.Role;
import it.vidiemme.school.business.model.User;
import it.vidiemme.school.business.validator.ValidationGroups.OnUpdate;

public class UserForm {
    @NotNull(groups = { OnUpdate.class })
    @Positive(groups = { OnUpdate.class })
    private Integer id;
    @NotEmpty
    @Length(min=0, max=250)
    @Email
    private String username;
    @NotEmpty
    @Length(min=0, max=250)
    private String firstName;
    @NotEmpty
    @Length(min=0, max=250)
    private String lastName;
    private Boolean isEnabled;
    private Date lastAccess;
    private Date createdAt;
    private Date updatedAt;
    @NotEmpty
    private List<Integer> roles; 
    
    public List<Integer> getRoles() {
        return roles;
    }
    public void setRoles(List<Integer> roles) {
        this.roles = roles;
    }
    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getUsername() {
        return username;
    } 
    public void setUsername(String username) {
        this.username = username;
    }
    public String getFirstName() {
        return firstName;
    }
    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }
    public String getLastName() {
        return lastName;
    }
    public void setLastName(String lastName) {
        this.lastName = lastName;
    }
    public Boolean getIsEnabled() {
        return isEnabled;
    }
    public void setIsEnabled(Boolean isEnabled) {
        this.isEnabled = isEnabled;
    }
    public Date getLastAccess() {
        return lastAccess;
    }
    public void setLastAccess(Date lastAccess) {
        this.lastAccess = lastAccess;
    }
    public Date getCreatedAt() {
        return createdAt;
    }
    public void setCreatedAt(Date createdAt) {
        this.createdAt = createdAt;
    }
    public Date getUpdatedAt() {
        return updatedAt;
    }
    public void setUpdatedAt(Date updatedAt) {
        this.updatedAt = updatedAt;
    }
    public User toUser(){
        User user = new User();
        user.setUsername(username);
        user.setFirstName(firstName);
        user.setLastName(lastName);
        user.setRoles(roles.stream().map(roleId ->{{ 
            Role role = new Role();
            role.setId(roleId);
            return role;
        }}).collect(Collectors.toList()));
        user.setId(id);
        user.setIsEnabled(isEnabled);
        user.setLastAccess(lastAccess);
        user.setCreatedAt(createdAt);
        user.setUpdatedAt(updatedAt); 
        return user;
    }
    
    public static UserForm fromUser(User user){
        if(user == null)
            return null;
        UserForm userForm = new UserForm();
        userForm.setUsername(user.getUsername());
        userForm.setFirstName(user.getFirstName());
        userForm.setLastName(user.getLastName());
        userForm.setId(user.getId());
        userForm.setRoles(user.getRoles().stream().map(Role::getId).collect(Collectors.toList()));
        userForm.setIsEnabled(user.getIsEnabled());
        userForm.setLastAccess(user.getLastAccess());
        userForm.setCreatedAt(user.getCreatedAt());
        userForm.setUpdatedAt(user.getUpdatedAt()); 
        return userForm;
    }
}
