package it.vidiemme.school.webapi.cucumber;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

/**
 * Cucumber tests
 * @author flavio.ricci
 *
 * Run smoke tests: mvn test -Dtest=CucumberTest -Dcucumber.options="--tags @Smoke"
 * Run integration tests: mvn test -Dtest=CucumberTest -Dcucumber.options="--tags @EndToEnd"
 */
@RunWith(Cucumber.class)
@CucumberOptions(
        features = "classpath:cucumber-features",
        plugin = { "pretty", "html:target/cucumber-reports" },
        glue = { "it.vidiemme.school.webapi.cucumber.steps" },
        tags = {"@EndToEnd"}
        )
public class CucumberTest {
    
}