/**
 * Initialize i18next
 */
$(function () {
    var lang = $('html').attr('lang');

    i18next.init({
        lng: lang,
        fallbackLng: 'en',
        defaultNS: 'boost',
    }).then(function() {
        bootbox.setDefaults({
            /**
             * @optional String
             * @default: en
             * which locale settings to use to translate the three
             * standard button labels: OK, CONFIRM, CANCEL
             */
            locale: lang,

            /**
             * @optional Boolean
             * @default: true
             * whether the dialog should be shown immediately
             */
            show: true,

            /**
             * @optional Boolean
             * @default: true
             * whether the dialog should be have a backdrop or not
             */
            backdrop: true,

            /**
             * @optional Boolean
             * @default: true
             * show a close button
             */
            closeButton: true,

            /**
             * @optional Boolean
             * @default: true
             * animate the dialog in and out (not supported in < IE 10)
             */
            animate: true,

            title: i18next.t('dialogs.warning'),

        });

        $('.chosen-select').chosen({
            width: "100%",
            allow_single_deselect: true,
        });
        $('.require-confirm').on('click', function (e) {
            e.preventDefault();
            openConfirmDelete($(this).data('message'), i18next.t('dialogs.confirm'), i18next.t('dialogs.cancel'), this);
            return false;
        });

        if ($('.table').length > 0) {
            $('body').on('click', 'table .actions-open-button', function () {
                $('.actions-container').hide();
                $('.actions-container', $(this).parent()).slideDown();
                return false;
            }).on('click', function () {
                $('.actions-container').slideUp();
            });
        }
    });
});

window.csrfToken = $('meta[name=\'_csrf\']').attr('content') || '';
window.csrfHeader = $('meta[name=\'_csrf_header\']').attr('content') || '';

$.ajaxSetup({
    error: function () {
        displayNetworkError();
    }
});

$.ajaxPrefilter(function (options, originalOptions, jqXHR) {
    var types = ['GET', 'HEAD', 'OPTIONS'];
    if( (!!originalOptions.type && !~types.indexOf(originalOptions.type)) ||
            (!!originalOptions.method && !~types.indexOf(originalOptions.method)) )
        jqXHR.setRequestHeader(window.csrfHeader, window.csrfToken);
});

$(function () {
    // Toastr configurations
    toastr.options = {
        "closeButton": true,
        "debug": false,
        "progressBar": false,
        "preventDuplicates": true,
        "positionClass": "toast-top-center",
        "onclick": null,
        "showDuration": "400",
        "hideDuration": "1000",
        "timeOut": "7000",
        "extendedTimeOut": "1000",
        "showEasing": "swing",
        "hideEasing": "linear",
        "showMethod": "fadeIn",
        "hideMethod": "fadeOut",
    };

    $('.chosen-select').chosen({width: "100%", allow_single_deselect: true});
    $('.require-confirm').on('click', function (e) {
        e.preventDefault();
        openConfirmDelete($(this).data('message'), 'Conferma', 'Annulla', this);
        return false;
    });

    if ($('.table').length > 0) {
        $('body').on('click', 'table .actions-open-button', function () {
            $('.actions-container').hide();
            $('.actions-container', $(this).parent()).slideDown();
            return false;
        }).on('click', function () {
            $('.actions-container').slideUp();
        });
    }
});
