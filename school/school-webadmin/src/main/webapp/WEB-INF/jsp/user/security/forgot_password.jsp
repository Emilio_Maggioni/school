<%@ include file="/includes/taglibs.jsp"%>
<head>
<title><spring:message code="user.resetpassword" /> <spring:message code="project.brand"/></title>
</head>
<body>
    <h2><spring:message code="user.forgotpassword" /></h2>
    <form:form class="m-t" method="post" modelAttribute="forgotPwdForm">
        <fieldset>
            <form:errors path="*" cssClass="alert alert-danger" element="div" cssStyle="margin-bottom:20px" />
            <div class="form-group">
                <spring:message code="user.resetpassword.email" var="msg_cp" />
                <form:input path="email" class="vdm-custom-inputtext form-control" type="text" placeholder="${msg_cp}"  required="required"/>
            </div>
            <div class="p-h-md">
                <button type="submit" class="btn btn-primary block full-width" ><spring:message code="confirm"/></button>
            </div>
        </fieldset>
    </form:form>
</body>

