package it.vidiemme.school.webadmin.web.validator;

import javax.validation.groups.Default;

public interface ValidationGroups {
    //validation groups interfaces
    public interface OnCreate extends Default {}
    public interface OnUpdate extends Default {}
    public interface OnDelete extends Default {}
}
