const packageJSON = require('../package');
const path = require('path');
const configPath = require('./path-webpack.config');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

const SOURCE_DIR = configPath.SOURCE_DIR;

/**
 *
 * @type {*[]}
 */
const getLoaders = (devMode = true) => [
    {
        test: /\.js$/,
        loaders: [
            {
                loader: 'babel-loader',
                options: {
                    presets: ['@babel/preset-env']
                }
            },
            {
                loader: 'eslint-loader',
                options: {
                    fix: true,
                },
            }
        ],
        exclude: /node_modules|vendor\/js/,
    },
    {
        test: /\.*(sass|[s]?css)$/,
        use: [
            MiniCssExtractPlugin.loader,
            {
                loader: 'css-loader',
                options: {
                    import: true
                },
            },
            'resolve-url-loader',
            {
                loader: 'postcss-loader',
                options: {
                    config: {
                        path: path.resolve(SOURCE_DIR, 'postcss.config.js'),
                    },
                    sourceMap: true
                },
            },
            {
                loader: 'sass-loader',
            },
        ],
    },
    {
        test: /\.(png|jp[e]?g|gif|svg)$/,
        loader: 'file-loader',
        options: {
            name: 'images/[name]-[hash].[ext]',
        },
    },
    {
        test: /\.(woff[2]?|eot|ttf|otf)$/,
        loader: 'file-loader',
        options: {
            name: 'fonts/[name]-[hash].[ext]',
        },
    },
    {
        test: /\.json$/,
        loader: 'json-loader',
    },
];


module.exports = (devMode) => {
    return getLoaders(devMode);
};
