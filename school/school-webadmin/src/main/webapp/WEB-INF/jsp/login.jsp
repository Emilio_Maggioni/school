<!DOCTYPE html>
<html lang="en">
    <%@include file="/includes/taglibs.jsp"%>
    <head>
        <title><spring:message code="login"/> <spring:message code="project.brand"/></title>
    </head>
    <body>
        <c:if test="${param.error == 'true'}">
            <div class="alert alert-danger">
                <strong><spring:message code="login.wrong-username-or-password" /></strong>
            </div>
        </c:if>
        <h3>Welcome to Boost</h3>
        <form class="m-t" role="form" action="${ctx}/login" method="POST">
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
            <div class="form-group text-left">
                <label for="username"><spring:message code="user.username"/></label>
                <input name="username" type="email" class="form-control" placeholder="Username" required="">
            </div>
            <div class="form-group text-left m-b-md">
                <label for="password"><spring:message code="user.password"/></label>
                <input name="password" type="password" class="form-control" placeholder="Password" required="">
            </div>
            <div class="p-h-md">
                <button type="submit" class="btn btn-primary block full-width"><spring:message code="login"/></button>
            </div>
            <a href="${ctx}/user/reset-password"><small><spring:message code="user.forgotpassword"/>?</small></a>
        </form>
    </body>
</html>
