package it.vidiemme.school.webapi.web.dto;

import java.util.List;
import java.util.stream.Collectors;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;

import org.apache.commons.lang.builder.ReflectionToStringBuilder;
import org.hibernate.validator.constraints.Length;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonInclude.Include;

import it.vidiemme.school.business.model.Role;
import it.vidiemme.school.business.model.User;

/**
 * DTO representing a user
 */
@JsonInclude(Include.NON_NULL)
public class UserDTO {

    private Integer id;

    @NotEmpty
    @Length(min = 0, max = 250)
    @Email
    private String username;

    @NotEmpty
    @Length(min = 0, max = 250)
    private String firstName;

    @NotEmpty
    @Length(min = 0, max = 250)
    private String lastName;

    @NotEmpty
    private List<Integer> roles;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public List<Integer> getRoles() {
        return roles;
    }

    public void setRoles(List<Integer> roles) {
        this.roles = roles;
    }

    @Override
    public String toString() {
        return ReflectionToStringBuilder.toString(this);
    }

    /**
     * Builds user model from user DTO
     * 
     * @param userDTO the user DTO
     * @return the User model instance
     */
    public User toUser() {
        User user = new User();
        user.setUsername(getUsername());
        user.setFirstName(getFirstName());
        user.setLastName(getLastName());
        user.setRoles(getRoles().stream().map(roleId -> {
            {
                Role role = new Role();
                role.setId(roleId);
                return role;
            }
        }).collect(Collectors.toList()));
        return user;
    }

    /**
     * Builds user DTO from user DB model
     * 
     * @param user the user DB model
     * @return the user DTO
     */
    public static UserDTO fromUser(User user) {
        UserDTO userDTO = new UserDTO();
        userDTO.setId(user.getId());
        userDTO.setUsername(user.getUsername());
        userDTO.setFirstName(user.getFirstName());
        userDTO.setLastName(user.getLastName());
        if (user.getRoles() != null) {
            userDTO.setRoles(user.getRoles().stream().map(r -> r.getId()).collect(Collectors.toList()));
        }
        return userDTO;
    }
}
