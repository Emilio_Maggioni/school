<%@include file="/includes/taglibs.jsp"%>
<html>
<head>
    <title><spring:message code="user.add"/> <spring:message code="project.brand"/></title>
</head>
<body>
    <c:set var="currentMenuItem" scope="request" value="manage-users"/>

    <div class="row border-bottom white-bg page-title">
        <div class="col-lg-10">
            <h2><spring:message code="user.manage"/></h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="${ctx}/user"><spring:message code="user.manage"/></a>
                </li>
                <li class="active breadcrumb-item">
                    <strong><spring:message code="user.add"/></strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-12">
                <div class="ibox" >
                    <div class="ibox-title">
                        <h5><spring:message code="user.list"/></h5>
                    </div>
                    <div class="ibox-content">
                        <form:form method="POST" action="${ctx}/user/add" modelAttribute="userForm" cssClass="form-with-validation">
                            <jsp:include page="_form.jsp"></jsp:include>
                        </form:form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>
