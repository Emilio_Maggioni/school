package it.vidiemme.school.webapi.web.error;

public class ApiGlobalError {
	private String code;
	private Object[] errorParams;

	public ApiGlobalError(String code, Object[] errorParams) {
		super();
		this.code = code;
		this.errorParams = errorParams;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

    public Object[] getErrorParams() {
        return errorParams;
    }

    public void setErrorParams(Object[] errorParams) {
        this.errorParams = errorParams;
    }
}
