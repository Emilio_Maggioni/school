import i18next from 'i18next';

import it from './it';
import en from './en';

/**
 * Inizializza la localizzazione
 *
 * @ignore
 */
export default function locales() {
    i18next.init({
        debug: (process.env.NODE_ENV === 'local' || process.env.NODE_ENV === 'development'),
        lng: 'it',
        fallbackLng: 'en',
        resources: {
            it: {
                translation: it,
            },
            en: {
                translation: en,
            },
        },
    });
}
