package it.vidiemme.school.webadmin.service;

import java.io.IOException;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.mail.MailException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import it.vidiemme.school.business.exception.ServiceException;
import it.vidiemme.school.business.model.User;
import it.vidiemme.school.business.service.EmailService;
import it.vidiemme.school.business.service.UserService;
import it.vidiemme.school.business.utils.Utils;

@Service
public class UsersEmailService {

    @Autowired
    private EmailService emailService;
    @Autowired
    private PasswordEncoder passwordEncoder;
    @Autowired
    private UserService userService;
    @Autowired
    private MessageSource message;

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    /** Sends the user the email with the link to reset his password
     * @param email the user email
     * @param req req the servlet request
     * @throws MailException
     * @throws MessagingException
     * @throws IOException
     * @throws AddressException
     */
    @Transactional(rollbackFor = Exception.class)
    public void sendResetPasswordMail(String email, HttpServletRequest req) throws ServiceException {

        User user = userService.getByUsername(email);

        if (user != null) {

            String s = user.getUsername() + System.currentTimeMillis();
            String token = passwordEncoder.encode(s);
            user.setToken(token);
            userService.updateUser(user);

            String setNewPwdUri = Utils.getApplicationAbsoluteUrl() + "/user/set-new-password?token=" + token;

            String subject = message.getMessage("user.setnewpwd.mail.subject", null, null);
            String body = message.getMessage("user.setnewpwd.mail.body", new String[] { setNewPwdUri }, null);

            try {
                emailService.sendEmailWithTemplate(new String[] { user.getUsername() }, subject, body, null);
            } catch (MessagingException | IOException e) {
                throw new ServiceException("The reset password email could not be sent", e);
            }
        }
    }

    /** Sends the created user the email to activate its credentials
     * @param user the new user
     * @param req the servlet request
     * @throws MailException
     * @throws MessagingException
     * @throws IOException
     * @throws AddressException
     */
    @Transactional(rollbackFor = Exception.class)
    public void sendUserActivationMail(User user, HttpServletRequest req) throws MailException, MessagingException, IOException, AddressException {

        String s = user.getUsername() + System.currentTimeMillis();
        String token = passwordEncoder.encode(s);
        user.setToken(token);
        userService.updateUser(user);

        String setNewPwdUri = Utils.getApplicationAbsoluteUrl() + "/user/set-new-password?token=" + token;

        String subject = message.getMessage("user.activation.mail.subject", null, null);
        String body = message.getMessage("user.activation.mail.body", new String[] { setNewPwdUri }, null);

        emailService.sendEmailWithTemplate(new String[] { user.getUsername() }, subject, body, null);
    }
}
