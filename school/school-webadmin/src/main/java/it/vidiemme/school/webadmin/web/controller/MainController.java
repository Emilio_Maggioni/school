/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package it.vidiemme.school.webadmin.web.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.Authentication;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/")
public class MainController {
    private static final Logger LOG = LoggerFactory.getLogger(MainController.class);

    @Autowired
    Environment env;
    
    @Autowired
    ControllerHelper controllerHelper;
    
    @GetMapping("/")
    public String home(Authentication authentication) {
        return "redirect:" + controllerHelper.getUserDefaultUrl(authentication);
    }

    @PreAuthorize("hasAuthority('EXAMPLE_SECTION_VIEW')")
    @GetMapping("/manage_users")
    public String exampleSectionView() {
        return "exampleSection";
    }
}
