<%@include file="/includes/taglibs.jsp"%>
<html>
<head>
    <title><spring:message code="user.show"/> <spring:message code="project.brand"/></title>
</head>
<body>
    <c:set var="currentMenuItem" scope="request" value="manage-users"/>

    <div class="row border-bottom white-bg page-title">
        <div class="col-lg-10">
            <h2><spring:message code="user.manage"/></h2>
            <ol class="breadcrumb">
                <li class="breadcrumb-item">
                    <a href="${ctx}/user"><spring:message code="user.manage"/></a>
                </li>
                <li class="active breadcrumb-item">
                    <strong><spring:message code="user.show"/></strong>
                </li>
            </ol>
        </div>
    </div>

    <div class="wrapper wrapper-content animated fadeInRight">
        <div class="row">
            <div class="col-12">
                <div class="ibox" >
                    <div class="ibox-title">
                        <h5><spring:message code="user.show"/></h5>
                    </div>
                    <div class="ibox-content">
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label" ><spring:message code="user.username"/></label>
                                    <div class="vdm-custom-label-input">${userForm.username}</div>
                                </div>
                            </div>
                                <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label" ><spring:message code="user.firstName"/></label>
                                    <div class="vdm-custom-label-input">${userForm.firstName}</div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label" ><spring:message code="user.lastName"/></label>
                                    <div class="vdm-custom-label-input"> ${userForm.lastName}</div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label" ><spring:message code="user.roles" /></label>
                                    <div class="vdm-custom-label-input">
                                    <select class="chosen-select form-control" multiple="multiple" disabled="disabled">
                                        <option label="---" value="" />
                                        <c:forEach items="${roleList}" var="role">
                                            <c:set value="" var="selected"></c:set>
                                            <c:forEach items="${userForm.roles}" var="userRole">
                                                <c:if test="${role.id == userRole}">
                                                    <c:set value="selected" var="selected"></c:set>
                                                </c:if>
                                            </c:forEach>
                                            <option value="${role.id}" ${selected}><spring:message code="${role.name}" /></option>
                                        </c:forEach>
                                    </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label" ><spring:message code="user.enabled"/></label>
                                    <div class="vdm-custom-label-input">
                                        <c:choose>
                                            <c:when test="${userForm.isEnabled}"><spring:message code="yes"/></c:when>
                                            <c:otherwise><spring:message code="no"/></c:otherwise>
                                        </c:choose>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label" ><spring:message code="createdAt"/></label>
                                    <div class="vdm-custom-label-input">
                                        <fmt:formatDate value="${userForm.createdAt}" dateStyle="full"  pattern="dd/MM/yyyy HH:mm" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label" ><spring:message code="updatedAt"/></label>
                                    <div class="vdm-custom-label-input">
                                        <fmt:formatDate value="${userForm.updatedAt}" dateStyle="full"  pattern="dd/MM/yyyy HH:mm" />
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label" ><spring:message code="user.lastAccess"/></label>
                                    <div class="vdm-custom-label-input">
                                        <fmt:formatDate value="${userForm.lastAccess}" dateStyle="full"  pattern="dd/MM/yyyy HH:mm" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
