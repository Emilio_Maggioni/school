package it.vidiemme.school.webapi.service;

import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

import com.fasterxml.jackson.core.JsonProcessingException;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import it.vidiemme.school.business.model.User;
import it.vidiemme.school.business.security.UserPrincipal;
import it.vidiemme.school.webapi.web.dto.JwtAuthenticationResponse;

@Component
public class JwtTokenService {

	private static final Logger logger = LoggerFactory.getLogger(JwtTokenService.class);

	private static final String CLAIM_KEY_ID = "key";
	private static final String CLAIM_KEY_USERNAME = "sub";
	private static final String CLAIM_KEY_AUDIENCE = "audience";
	private static final String CLAIM_KEY_CREATED = "iat";
	private static final String CLAIM_KEY_AUTHORITIES = "privileges";
	private static final String CLAIM_KEY_IS_ENABLED = "isEnabled";

	private static final String AUDIENCE_WEB = "web";

	@Value("${jwt.secret}")
	private String secret;

	@Value("${jwt.expiration}")
	private Long expiration;

	@Autowired
	private AuthenticationManager authenticationManager;

	@Autowired
	private UserDetailsService userDetailsService;

	/**
	 * Login and generate the JWT
	 * 
	 * @param username
	 * @param password
	 * @return the response containing the JWT
	 * @throws JsonProcessingException
	 */
	public JwtAuthenticationResponse login(String username, String password) {
		final Authentication authentication = authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
		SecurityContextHolder.getContext().setAuthentication(authentication);
		final UserDetails userDetails = userDetailsService.loadUserByUsername(username);
		final String token = generateToken(userDetails);

		return new JwtAuthenticationResponse(token);
	}

	/**
	 * Retrieves the user details from the passed-in JWT
	 * 
	 * @param token the input JWT
	 * @return the user details
	 */
	@SuppressWarnings("unchecked")
	public UserPrincipal getUserDetails(String token) {
		if (token == null) {
			logger.debug("JWT is empty (maybe the route does not need authentication)");
			return null;
		}
		final Claims claims = getClaimsFromToken(token);
		if (claims == null) {
			logger.warn(String.format("JWT received %s but claims are empty", token));
			return null;
		}
		
		Collection<GrantedAuthority> authorities = null;
		List<String> privileges = null;
		if (claims.get(CLAIM_KEY_AUTHORITIES) != null) {
			// @formatter:off
			authorities = ((List<String>) claims.get(CLAIM_KEY_AUTHORITIES)).stream()
					.map(auth -> new SimpleGrantedAuthority(auth)).collect(Collectors.toList());
			// @formatter:on
		}

		User user = new User();
		user.setUsername(claims.getSubject());
		user.setIsEnabled((Boolean) claims.get(CLAIM_KEY_IS_ENABLED));
		user.setId((Integer) claims.get(CLAIM_KEY_ID));
		privileges = (List<String>) claims.get(CLAIM_KEY_AUTHORITIES);
		user.setPrivileges(privileges);
		UserPrincipal userPrincipal = new UserPrincipal(user, authorities);
		return userPrincipal;

	}

	/**
	 * Generate a new JWT
	 * 
	 * @param token the JWT to be refreshed
	 * @return the refreshed JWT
	 */
	public String refreshToken(String token) {
		String refreshedToken;
		final Claims claims = getClaimsFromToken(token);
		claims.put(CLAIM_KEY_CREATED, new Date());
		refreshedToken = generateToken(claims);
		return refreshedToken;
	}

	/**
	 * Validate the passed-in JWT
	 * 
	 * @param token       the JWT
	 * @param userDetails the user details
	 * @return true if the passed-in token is valid; false otherwise
	 */
	public boolean isTokenValid(String token, UserDetails userDetails) {
		UserPrincipal user = (UserPrincipal) userDetails;
		final String username = getUsernameFromToken(token);
		return (username.equals(user.getUsername()) && !isTokenExpired(token));
	}

	private String getUsernameFromToken(String token) {
		final Claims claims = getClaimsFromToken(token);
		String username = claims.getSubject();
		logger.debug(String.format("Username read from token: %s", username));
		return username;
	}

	private Date getExpirationDateFromToken(String token) {
		Date expiration;
		final Claims claims = getClaimsFromToken(token);
		expiration = claims.getExpiration();
		return expiration;
	}

	private Claims getClaimsFromToken(String token) {
		Claims claims = Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
		return claims;
	}

	private Date generateExpirationDate() {
		return new Date(System.currentTimeMillis() + expiration * 1000);
	}

	private boolean isTokenExpired(String token) {
		final Date expiration = getExpirationDateFromToken(token);
		return expiration.before(new Date());
	}

	private String generateAudience() {
		return AUDIENCE_WEB;
	}

	private String generateToken(UserDetails userDetails) {
		Map<String, Object> claims = new HashMap<>();
		claims.put(CLAIM_KEY_ID, ((UserPrincipal) userDetails).getUser().getId());
		claims.put(CLAIM_KEY_USERNAME, userDetails.getUsername());
		claims.put(CLAIM_KEY_AUDIENCE, generateAudience());
		claims.put(CLAIM_KEY_CREATED, new Date());
		List<String> authorities = userDetails.getAuthorities().stream()
				.map(auth -> auth.getAuthority())
				.collect(Collectors.toList());
		claims.put(CLAIM_KEY_AUTHORITIES, authorities);
		claims.put(CLAIM_KEY_IS_ENABLED, userDetails.isEnabled());

		return generateToken(claims);
	}

	private String generateToken(Map<String, Object> claims) {
		return Jwts.builder().setClaims(claims).setExpiration(generateExpirationDate()).signWith(SignatureAlgorithm.HS256, secret).compact();
	}
	
}
