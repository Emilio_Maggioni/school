package it.vidiemme.school.webapi.config;

import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.web.WebAppConfiguration;

/**
 * Base jUnit test class to be extended by jUnit tests which need Spring context initialization
 * @author flavio.ricci
 */
@WebAppConfiguration
@TestPropertySource(locations = {
		"classpath:properties/app.properties", 
		"classpath:properties/mail.properties", 
		"classpath:properties/jdbc.properties"}
)
@ContextConfiguration(classes = {TestAppConfig.class, WebConfig.class, TestRepositoryConfig.class})
public class JUnitTestBase {

}
