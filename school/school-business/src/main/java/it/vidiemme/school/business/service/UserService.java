package it.vidiemme.school.business.service;

import java.sql.Timestamp;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import it.vidiemme.school.business.dao.RoleMapper;
import it.vidiemme.school.business.dao.UserMapper;
import it.vidiemme.school.business.dto.DataTableRequestDTO;
import it.vidiemme.school.business.dto.DataTableResponseDTO;
import it.vidiemme.school.business.exception.ServiceException;
import it.vidiemme.school.business.filter.UserFilter;
import it.vidiemme.school.business.model.Role;
import it.vidiemme.school.business.model.RoleExample;
import it.vidiemme.school.business.model.User;
import it.vidiemme.school.business.model.UserExample;
import it.vidiemme.school.business.security.UserPrincipal;
import it.vidiemme.school.business.utils.PasswordUtils;

@Service
public class UserService {

    @Autowired
    private UserMapper userMapper;
    @Autowired
    private RoleMapper roleMapper;
    @Autowired(required = false)
    private PasswordEncoder passwordEncoder;

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    /**
     * Gets all users
     *
     * @return all users
     */
    public List<User> getAll() {
        UserExample example = new UserExample();
        example.createCriteria().andIsDeletedEqualTo(Boolean.FALSE);
        return userMapper.selectByExample(example);
    }

    /**
     * Get all user by filter
     * @return list users
     */
    public DataTableResponseDTO<List<User>> getAllFilteredAndPaginated(DataTableRequestDTO<UserFilter> dataTableRequestDTO) {
        List<User> users = userMapper.selectAllFilteredPaginated(dataTableRequestDTO);
        Integer totalUser = getAllFilteredTotal(dataTableRequestDTO).size();
        DataTableResponseDTO<List<User>> result = new DataTableResponseDTO<List<User>>();
        result.setData(users);
        result.setDraw(dataTableRequestDTO.getDraw());
        result.setRecordsFiltered(totalUser);
        result.setRecordsTotal(totalUser);
        return result;
    }

    /**
     * Get all user by filter
     * @return list users
     */
    public List<User> getAllFilteredTotal(DataTableRequestDTO<UserFilter> dataTableRequestDTO) {
        List<User> users = userMapper.selectAllFilteredTotal(dataTableRequestDTO);
        return users;
    }

    /**
     * Gets the user by id.
     *
     * @param id
     * @return user
     */
    public User getById(Integer id, Boolean complete) {
        if (complete)
            return userMapper.selectByIdWithRolesAndPrivileges(id);

        User user = userMapper.selectByPrimaryKey(id);
        return user == null || user.getIsDeleted() ? null : user;
    }

    /**
     * Gets the by username.
     * 
     * @param username the username
     * @return the user by username
     */
    public User getByUsername(String username) {
        UserExample example = new UserExample();
        example.createCriteria().andUsernameEqualTo(username).andIsDeletedEqualTo(Boolean.FALSE);
        List<User> ul = userMapper.selectByExample(example);
        return (!ul.isEmpty()) ? ul.get(0) : null;
    }

    /**
     * Create user 
     * 
     * @param user
     * @return new user
     */
    @Transactional(rollbackFor = Exception.class)
    public User addUser(User user, List<Integer> roles) {

        String pwd = PasswordUtils.generateRandomPassword();

        user.setIsEnabled(Boolean.FALSE);
        user.setIsDeleted(Boolean.FALSE);
        user.setPassword(passwordEncoder.encode(pwd));
        user.setTempPassword(Boolean.TRUE);
        userMapper.insertSelective(user);
        roles.forEach(role -> {
            {
                userMapper.insertUserRole(user.getId(), role);
            }
        });
        return user;
    }

    /**
     * Update user 
     * 
     * @param user
     */
    @Transactional(rollbackFor = Exception.class)
    public void update(User user, List<Integer> roles) {

        if (user != null) {
            // update record
            userMapper.updateByPrimaryKeySelective(user);
            //update roles
            userMapper.deleteUserRolesByUserId(user.getId());
            roles.forEach(role -> {
                {
                    userMapper.insertUserRole(user.getId(), role);
                }
            });
        }
    }

    /**
     * Update user last access date
     * 
     * @param user
     */
    @Transactional(rollbackFor = Exception.class)
    public void updateLastAccess(User user) {

        if (user != null) {
            // update record
            user.setLastAccess(new Date());
            userMapper.updateByPrimaryKeySelective(user);
        }
    }

    @Transactional(rollbackFor = Exception.class)
    public void updateUser(User user) {
        if (user != null) {
            userMapper.updateByPrimaryKeySelective(user);
        }
    }

    /**
     * Logical user deletion 
     * @param user
     */
    @Transactional(rollbackFor = Exception.class)
    public void delete(Integer userId) {
        if (userId != null) {
            User user = new User();
            user.setId(userId);
            user.setIsDeleted(Boolean.TRUE);
            userMapper.updateByPrimaryKeySelective(user);
        }
    }

    /**
     * Check if email is already used in username field  
     * @param username
     * @param userToBeExcludedId
     * @return Boolean 
     */
    public Boolean isEmailAlreadyUsed(String username, Integer userToBeExcludedId) {
        return userMapper.checkIfEmailIsAlreadyUsed(username, userToBeExcludedId);
    }

    /**
     * Get current authenticated user by context
     * @return User
     */
    public User getCurrentLoggedUser() {
        UserPrincipal userPricipal = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return userPricipal.getUser();
    }

    /**
     * Select user by token
     * @param token
     * @return
     */
    public User getByToken(String token) {
        UserExample ex = new UserExample();
        ex.createCriteria().andTokenEqualTo(token).andIsDeletedEqualTo(Boolean.FALSE);
        List<User> users = userMapper.selectByExample(ex);
        Assert.isTrue(users.size() <= 1, "unexpected result size");
        return (users != null && users.size() == 1) ? users.get(0) : null;
    }

    /**
     * Set new password after user activation
     * @param token
     * @param newPassword
     */
    @Transactional(rollbackFor = Exception.class)
    public User setNewPassword(String token, String newPassword) {
        User user = getByToken(token);
        if (user == null)
            throw new ServiceException("No user found with the specified reset password token");
        
        log.info("Reset password per l'utente ID: " + user.getId());

        String newEncodedPassword = passwordEncoder.encode(newPassword);

        long now = System.currentTimeMillis();
        Timestamp original = new Timestamp(now);
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(original.getTime());
        cal.add(Calendar.MONTH, 3);
        if (user.getPassword() == null || (user.getPassword() != null && !user.getPassword().equals(newEncodedPassword))) {
            user.setPassword(newEncodedPassword);
            userMapper.updateByPrimaryKeySelective(user);
        }
        reactivateUser(user.getId());
        log.info("Reset password per l'utente ID: " + user.getId() + " avvenuto correttamente");
        return user;
    }

    /**
     * update user and set is_enable = true (used after activation)
     * @param userId
     */
    @Transactional(rollbackFor = Exception.class)
    public void reactivateUser(Integer userId) {
        userMapper.reactivateUser(userId);
    }

    /**
     * set temporary password to user given its id
     * @param id
     */
    @Transactional(rollbackFor = Exception.class)
    public void resetPassword(Integer id) {
        User user = userMapper.selectByPrimaryKey(id);
        user.setTempPassword(Boolean.TRUE);
        user.setUpdatedAt(new Date());
        userMapper.updateByPrimaryKeySelective(user);
    }

    /**
     * Set new password to authenticated user 
     * @param newPassword
     */
    @Transactional(rollbackFor = Exception.class)
    public void changePassword(String newPassword) {
        UserPrincipal userPrincipal = (UserPrincipal) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        User user = userPrincipal.getUser();
        user.setPassword(passwordEncoder.encode(newPassword));
        user.setTempPassword(Boolean.FALSE);
        user.setUpdatedAt(new Date());
        userMapper.updateByPrimaryKeySelective(user);
    }

    /**
     * select all roles
     * @return all role
     */
    public List<Role> getAllRoles() {
        RoleExample ex = new RoleExample();
        ex.setOrderByClause("name");
        return roleMapper.selectByExample(ex);
    }
}
