module.exports = ({ file, options, env }) => ({
    plugins: {
        'postcss-import': {},
        'postcss-cssnext': {
            'browsers': [
                '> 1%',
                'last 2 versions',
            ],
        },
    },
});
