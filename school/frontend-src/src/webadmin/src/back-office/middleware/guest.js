import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Route, Redirect } from 'react-router-dom';

/**
 * Guest middleware
 *
 * Mappa un componente su una Route e ne effettua il render solo se l'utente non è autenticato.
 * In caso contrario effettua un redirect su `/`.
 */
class Guest extends React.Component {
    render() {
        if (this.props.authenticated === true) {
            return <Redirect to='/' />;
        }
        return <Route {...this.props} />;
    }
}
Guest.propTypes = {
    authenticated: PropTypes.bool,
};

const mapStateToProps = state => ({
    authenticated: state.authentication.authenticated,
});

export default connect(mapStateToProps)(Guest);
