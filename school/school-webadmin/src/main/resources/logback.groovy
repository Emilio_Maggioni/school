import static ch.qos.logback.classic.Level.DEBUG
import static ch.qos.logback.classic.Level.ERROR
import static ch.qos.logback.classic.Level.INFO
import static ch.qos.logback.classic.Level.TRACE
import ch.qos.logback.classic.PatternLayout
import ch.qos.logback.classic.encoder.PatternLayoutEncoder 
import ch.qos.logback.core.ConsoleAppender
import ch.qos.logback.core.FileAppender
import ch.qos.logback.core.rolling.RollingFileAppender
import ch.qos.logback.core.rolling.TimeBasedRollingPolicy
import ch.qos.logback.classic.filter.ThresholdFilter

appender("CONSOLE", ConsoleAppender) {
  encoder(PatternLayoutEncoder) {
    pattern = "%date{ISO8601} [%thread] %-5level %logger{36} - %msg%n"
  }
}

appender("FILE", FileAppender) {
  file = "${logs.folder}/webadmin.log"
  append = true
  encoder(PatternLayoutEncoder) {
    pattern = "%date{ISO8601} [%thread] %-5level %logger{36} - %msg%n"
  }
}

logger("org.springframework", INFO)
logger("org.mybatis.spring", ERROR)
logger("org.apache.ibatis", INFO)
logger("it.vidiemme.school.business", DEBUG)
logger("it.vidiemme.school.business.dao", INFO)
logger("it.vidiemme.school.webadmin", DEBUG)

root(${log.level.root})