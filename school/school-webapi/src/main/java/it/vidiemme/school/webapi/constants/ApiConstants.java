package it.vidiemme.school.webapi.constants;

/**
 * API constants
 * @author flavio.ricci
 */
public class ApiConstants {

    public static final Integer API_ERROR_CODE = 1;
    
    
    /**
     * Prevent external initialization
     */
    private ApiConstants() {
    };

}

