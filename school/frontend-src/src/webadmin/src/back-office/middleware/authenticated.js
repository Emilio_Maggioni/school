import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Route, Redirect } from 'react-router-dom';

/**
 * Authenticated middleware
 *
 * Mappa un componente su una Route e ne effettua il render solo se l'utente è autenticato.
 * In caso contrario effettua un redirect su `/login`.
 */
class Authenticated extends React.Component {
    render() {
        if (this.props.authenticated === true) {
            return <Route {...this.props} />;
        }
        return <Redirect to='/login' />;
    }
}
Authenticated.propTypes = {
    authenticated: PropTypes.bool,
};

const mapStateToProps = state => ({
    authenticated: state.authentication.authenticated,
});

export default connect(mapStateToProps)(Authenticated);
