package it.vidiemme.school.webapi.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Tag;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import it.vidiemme.school.business.model.Role;
import it.vidiemme.school.business.model.User;
import it.vidiemme.school.business.service.UserService;
import it.vidiemme.school.webapi.config.JUnitTestBase;

@DisplayName("User Service tested on H2 DB")
@Tag("DB")
@ExtendWith(SpringExtension.class)
public class UserServiceTest extends JUnitTestBase {
	
	@Autowired
	private UserService userService;
	
	@Test
	public void testUserCRUDOperations() {
		final String userName = "test_user@vidiemme.it";
		final String firstName = "First";
		final String lastName = "Last";
		Role role = new Role();
		role.setId(1);
		final List<Role> roles = new ArrayList<>();
		roles.add(role);
		final List<Integer> rolesInt = roles.stream().map(r -> r.getId()).collect(Collectors.toList());
		
		User user = new User();
		user.setUsername(userName);
		user.setFirstName(firstName);
		user.setLastName(lastName);
		user.setRoles(roles);
		
		List<User> userListBefore = userService.getAll();
		
		//test create, get and list
		User createdUser = userService.addUser(user, rolesInt);
		User retrievedUser = userService.getById(createdUser.getId(), Boolean.TRUE);
		assertEquals(createdUser.getId(), retrievedUser.getId());
		assertEquals(createdUser.getUsername(), retrievedUser.getUsername());
		assertEquals(createdUser.getFirstName(), retrievedUser.getFirstName());
		assertEquals(createdUser.getLastName(), retrievedUser.getLastName());
		
		List<User> userListAfterCreate = userService.getAll();
		assertEquals(userListBefore.size() + 1, userListAfterCreate.size());
		
		//test update
		retrievedUser.setFirstName("UPDATED FIRST NAME");
		userService.update(retrievedUser, rolesInt);
		User retrievedUserUpdated = userService.getById(retrievedUser.getId(), Boolean.TRUE);
		assertEquals(retrievedUserUpdated.getFirstName(), "UPDATED FIRST NAME");
		
		//test delete
		userService.delete(retrievedUserUpdated.getId());
		List<User> userListAfterDelete = userService.getAll();
		assertEquals(userListBefore.size(), userListAfterDelete.size());
	}

}
