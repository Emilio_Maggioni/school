import CustomErrorWithError from '../../utilities/custom-error-with-data';

const defaultParameters = {
    method: 'GET',
    url: {
        url: '',
        path: {},
        query: {},
    },
    headers: {},
    form: null,
    json: null,
    body: null,
    responseType: 'text',
};

/**
 * Crea richieste ajax
 *
 * @param {Object} parameters
 */
class AjaxRequest {
    constructor(parameters = {}) {
        const params = AjaxRequest.normalizeParameters(parameters);

        /* METHOD */
        this.method = params.method.toLocaleUpperCase();

        /* HEADER */
        this.headers = {
            'X-Requested-With': params.headers['X-Requested-With'] || 'XMLHttpRequest',
            'Cache-Control': params.headers['Cache-Control'] || 'no-cache',
        };
        if (params.headers['Content-Type']) {
            if (params.headers['Content-Type'] !== 'multipart/form-data') {
                this.headers['Content-Type'] = params.headers['Content-Type'];
            }
        } else if (params.form) {
            this.headers['Content-Type'] = 'application/x-www-form-urlencoded';
        } else if (params.json) {
            this.headers['Content-Type'] = 'application/json';
        }

        /* BODY */
        if (params.form) {
            this.body = AjaxRequest.preparePOSTParameters(params.form);
        } else if (params.json) {
            this.body = JSON.stringify(params.json);
        } else if (params.body) {
            this.body = params.body;
        }

        /* URL */
        this.url = AjaxRequest.preparePathParameters(params.url.url, params.url.path);
        this.url = AjaxRequest.prepareQueryParameters(this.url, params.url.query);

        /* RESPONSE TYPE */
        this.responseType = params.responseType;

        /* XHR */
        this.xhr = new XMLHttpRequest();
        this.xhr.open(this.method, this.url);
        this.xhr.responseType = this.responseType;
        Object.keys(this.headers).forEach((header) => {
            this.xhr.setRequestHeader(header, this.headers[header]);
        });
    }

    /**
     * Esegue la chiamata al server
     *
     * @returns {Promise}
     */
    send() {
        return new Promise((resolve, reject) => {
            this.xhr.onreadystatechange = () => {
                if (this.xhr.readyState === 4) {
                    if (this.xhr.status > 101 && this.xhr.status < 207) {
                        resolve({
                            status: 'success',
                            code: this.xhr.status,
                            data: this.xhr.response,
                            xhr: this.xhr,
                        });
                    } else {
                        reject(new CustomErrorWithError({
                            status: 'fail',
                            code: this.xhr.status,
                            data: this.xhr.response,
                            xhr: this.xhr,
                        }, this.xhr.responseText));
                    }
                }
            };

            this.xhr.send(this.body);
        });
    }

    /**
     * Termina la chiamata al server
     */
    abort() {
        this.xhr.abort();
    }

    /**
     * Sostituisce i placeholder all'interno dell'URL
     *
     * @param {string} url
     * @param {Object} pathParameters
     * @returns {string} URL
     */
    static preparePathParameters(url, pathParameters) {
        let preparedURL = url;
        Object.keys(pathParameters).forEach((key) => {
            preparedURL = preparedURL.replace(`{${key}}`, pathParameters[key]);
        });
        return preparedURL;
    }

    /**
     * Concatena i parametri in formato encoded URI Components
     *
     * @param {Object} parameters
     * @returns {string} I parametri concatenati
     */
    static concatFormParameters(parameters) {
        const data = [];
        Object.keys(parameters).forEach((key) => {
            if (parameters[key] !== undefined) {
                data.push(`${key} = ${encodeURIComponent(parameters[key])}`);
            }
        });
        return data.join('&');
    }

    /**
     * Restituisce una string nel formato URL + query string
     *
     * @param {string} url
     * @param {Object} queryParameters
     * @returns {string} URL comprensivo di query string
     */
    static prepareQueryParameters(url, queryParameters) {
        const data = this.concatFormParameters(queryParameters);
        if (data.length === 0) {
            return url;
        }
        return `${url}?${data}`;
    }

    /**
     * @param {Object} postParameters
     * @returns {string} I parametri concatenati
     */
    static preparePOSTParameters(postParameters) {
        return this.concatFormParameters(postParameters);
    }

    /**
     *
     * @param {Object} params i parametri da normalizzare
     * @returns {Object} params normalizzato
     */
    static normalizeParameters(params = {}) {
        const normalizedParameters = Object.assign({}, defaultParameters, params);
        normalizedParameters.url = Object.assign({}, defaultParameters.url, params.url);
        normalizedParameters.url.path = Object.assign({}, defaultParameters.url.path, params.url.path);
        normalizedParameters.url.query = Object.assign({}, defaultParameters.url.query, params.url.query);
        normalizedParameters.headers = Object.assign({}, defaultParameters.headers, params.headers);
        return normalizedParameters;
    }
}

export default AjaxRequest;
