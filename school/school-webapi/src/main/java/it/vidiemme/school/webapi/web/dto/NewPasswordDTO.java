package it.vidiemme.school.webapi.web.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

public class NewPasswordDTO {

    @NotEmpty
    @Email
	private String email;
    @NotEmpty
    private String token;
    @Pattern(regexp="((?=.*[0-9])(?=.*[a-zA-Z]).{8,20})")
    private String newPassword;

	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
    public String getToken() {
        return token;
    }
    public void setToken(String token) {
        this.token = token;
    }
    public String getNewPassword() {
        return newPassword;
    }
    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }
}
