package it.vidiemme.school.webapi.web.filter;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;

import it.vidiemme.school.webapi.service.JwtTokenService;

public class AuthenticationTokenFilter extends UsernamePasswordAuthenticationFilter {

    @Autowired
    private JwtTokenService jwtTokenUtil;
    
    @Value("${jwt.header}")
    private String tokenHeader;

    @Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        if (request instanceof HttpServletRequest) {
            HttpServletRequest httpRequest = (HttpServletRequest) request;
            String token = httpRequest.getHeader(tokenHeader);
            UserDetails userDetails = jwtTokenUtil.getUserDetails(token);
            
            addAuthentcationDetailsToSecurityContext(httpRequest, token, userDetails);
            refreshToken(response, token);
            
            chain.doFilter(request, response);
        } else {
            throw new ServletException();
        }
    }

    /**
     * Validates the user token and eventually sets the token in the authentication context
     * @param httpRequest
     * @param authToken
     * @param userDetails
     */
    private void addAuthentcationDetailsToSecurityContext(HttpServletRequest httpRequest, String authToken, UserDetails userDetails) {
        if (userDetails != null && userDetails.getUsername() != null && SecurityContextHolder.getContext().getAuthentication() == null && jwtTokenUtil.isTokenValid(authToken, userDetails)) {
            UsernamePasswordAuthenticationToken authentication = new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities());
            authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(httpRequest));
            SecurityContextHolder.getContext().setAuthentication(authentication);
        }
    }

    /**
     * Refresh passed-in JWT
     * @param response
     * @param authToken
     */
    private void refreshToken(ServletResponse response, String authToken) {
        if (authToken != null) {
            HttpServletResponse httpResponse = (HttpServletResponse) response;
            String refreshedToken = jwtTokenUtil.refreshToken(authToken);
            httpResponse.setHeader(tokenHeader, refreshedToken);
            httpResponse.setHeader("Content-Type", "application/json; charset=utf-8");
        }
    }

}
