const path = require('path');
const packageJSON = require('./package.json');
const devMode = process.env.NODE_ENV !== 'production'

/**
 * Webpack Modules
 */
const pathConfig = require('./webpack-modules/path-webpack.config');
const loaders = require('./webpack-modules/loaders-webpack.config');
const plugins = require('./webpack-modules/plugins-webpack.config');
const entry = require('./webpack-modules/entry-webpack.config');

const project_name = packageJSON.name;
const default_projects = Object.keys(packageJSON.project_config.projects).map(el => el);

module.exports = env => {

    const PROFILE = env.profile.toLowerCase();
    const WATCH = env.watch;
    const ENV_PLATFORMS = env.platforms;
    const ENV_PROJECTS = env.projects;

    let projects = [];
    let platforms = [];

    if (!ENV_PROJECTS || ENV_PROJECTS === "false") {
        projects = default_projects;
    } else {
        projects = ENV_PROJECTS.split(',');
    }


    let webpack_conf = [];

    projects.map((project) => {

        if (!ENV_PLATFORMS || ENV_PLATFORMS === "false") {
            platforms = packageJSON.project_config.projects[project].platforms;
        } else {
            platforms = ENV_PLATFORMS.split(',');
        }

        platforms.map((platform) => {

            const build_path = path.resolve(
                '..',
                packageJSON.name + '-' + project,
                packageJSON.project_config.projects[project].build_path.join(path.sep)
            );

            webpack_conf.push({
                watch: WATCH && WATCH !== "false",
                entry: entry(PROFILE, project, platform),
                stats: (!devMode) ? 'errors-only' : 'minimal',
                mode: devMode ? 'development' : 'production',
                output: {
                    path: path.resolve(build_path,
                        platform
                    ),
                    filename: '[name].js',

                },
                resolve: {
                    alias: {
                        'config$': path.resolve(pathConfig.APP_DIR, platform, 'config.' + PROFILE + '.js'),
                    },
                    extensions: ['.js', '.json'],
                },
                module: {
                    rules: loaders(devMode, project, platform),
                },
                plugins: plugins(PROFILE, project, platform),
                devtool: ((PROFILE === 'local' || PROFILE === 'development') ? 'source-map' : undefined),
            });

            return null;
        });

        return null;
    });

    return webpack_conf;
};
