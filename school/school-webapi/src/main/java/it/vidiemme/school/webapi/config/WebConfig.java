package it.vidiemme.school.webapi.config;

import java.io.IOException;
import java.util.Locale;
import java.util.TimeZone;

import javax.validation.Validator;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScan.Filter;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.env.Environment;
import org.springframework.core.io.FileSystemResource;
import org.springframework.http.HttpMethod;
import org.springframework.validation.beanvalidation.LocalValidatorFactoryBean;
import org.springframework.validation.beanvalidation.MethodValidationPostProcessor;
import org.springframework.web.multipart.MultipartResolver;
import org.springframework.web.multipart.commons.CommonsMultipartResolver;
import org.springframework.web.servlet.LocaleResolver;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.springframework.web.servlet.i18n.CookieLocaleResolver;

/**
 * Configures the application MVC components.
 */
@Configuration
@EnableWebMvc
@ComponentScan(basePackages = { 
		"it.vidiemme.school.webapi.validator", 
		"it.vidiemme.school.webapi.controller", 
		"it.vidiemme.school.webapi.service",
        "it.vidiemme.school.webapi.security",
        "it.vidiemme.school.webapi.filter",
        "it.vidiemme.school.webapi.error",
        "it.vidiemme.school.webapi.dto",
        "it.vidiemme.school.webapi",
        "it.vidiemme.school.business.service" },
        excludeFilters=@Filter(IgnoreDuringComponentScan.class)
        )
public class WebConfig implements WebMvcConfigurer {
    @Autowired
    private Environment env;    
   
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        //path to the application static version file (generated by Jenkins build)
        registry.addResourceHandler("/version.txt").addResourceLocations("/version.txt");
    }

    /** The bean handles the multipart file upload resolution.
     * @return the MultipartResolver bean
     * @throws IOException 
     */
    @Bean
    public MultipartResolver multipartResolver() throws IOException {

        long maxFileUploadSize = Long.valueOf(env.getProperty("file.upload.size.max"));
        long maxMultipartFileUploadSize = Long.valueOf(env.getProperty("file.upload.multipart.max"));
        String uploadLocation = env.getProperty("file.upload.dir");

        CommonsMultipartResolver multipartResolver = new CommonsMultipartResolver();
        multipartResolver.setDefaultEncoding("utf-8");
        multipartResolver.setMaxUploadSize(maxMultipartFileUploadSize);
        multipartResolver.setMaxUploadSizePerFile(maxFileUploadSize);
        multipartResolver.setUploadTempDir(new FileSystemResource(uploadLocation));
        return multipartResolver;
    }

    /** The bean handles the strategy to determine the navigation locale, and sets the default locale and timezone.
     * @return the LocaleResolver bean
     */
    @Bean
    public LocaleResolver localeResolver() {
        CookieLocaleResolver bean = new CookieLocaleResolver();
        bean.setDefaultLocale(Locale.ITALIAN);
        bean.setDefaultTimeZone(TimeZone.getTimeZone("Europe/Rome"));
        return bean;
    }
    
    @Bean
    public MethodValidationPostProcessor methodValidationPostProcessor() {
        MethodValidationPostProcessor methodValidationPostProcessor = new MethodValidationPostProcessor();
        methodValidationPostProcessor.setValidator(basicValidator());
        return methodValidationPostProcessor;
    }

    @Bean
    public Validator basicValidator() {
        return new LocalValidatorFactoryBean();
    }    
    
    /**
     * Enable CORS for all routes. If you want specific CORS for each controller or route, use 
     * @CrossOrigin annotation
     */
    @Override
    public void addCorsMappings(CorsRegistry registry) {
        registry.addMapping("/**")
	        .allowedOrigins("*")
	        .allowedMethods(HttpMethod.GET.name(), HttpMethod.POST.name(), HttpMethod.PUT.name(), HttpMethod.DELETE.name(), HttpMethod.OPTIONS.name(), HttpMethod.PATCH.name())
	        .allowedHeaders("Access-Control-Allow-Origin", "Access-Control-Allow-Credentials", "Content-Type", "Cache-Control", "X-Requested-With", "Authorization", "X-Total-Count")
	        .exposedHeaders("Access-Control-Allow-Origin", "Access-Control-Allow-Credentials", "Authorization", "X-Total-Count")
	        .allowCredentials(false)
	        .maxAge(4800);
    }
    
}