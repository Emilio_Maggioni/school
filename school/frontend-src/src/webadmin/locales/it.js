i18next.on('initialized', () => {
    i18next.addResourceBundle('it', 'boost', {
        errors: {
            networkError: "Errore di rete",
        },
    }, true, true);

    i18next.addResourceBundle('it', 'boost', {
        dialogs: {
            error: "Errore",
            warning: "Attenzione",
            confirm: "Conferma",
            cancel: "Annulla",
        },
    }, true, true);

    i18next.addResourceBundle('it', 'boost', {
        welcome: {
            welcome: "Benvenuto {{name}}!",
            randomizeName: "Cambia nome",
        },
    }, true, true);
});
