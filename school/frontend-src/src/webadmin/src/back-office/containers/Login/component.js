import React from 'react';
import i18next from 'i18next';

import './index.scss';

import BaseComponent from '../BaseContainer/component';

/**
 * Login container
 */
class LoginComponent extends BaseComponent {
    constructor() {
        super();

        this.state = {
            username: '',
            password: '',
        };

        this.handleChange = this.handleChange.bind(this);
        this.buttonClick = this.buttonClick.bind(this);
    }

    /**
     * Gestisce i campi di login
     *
     * @param {Event} e Evento change
     */
    handleChange(e) {
        this.setState({
            [e.target.name]: e.target.value,
        });
    }

    buttonClick() {
        this.setState({ });
        print('pippo');
        console.log($('#page-wrapper'));
    }

    render() {
        return (
            <div className="container-fluid">
                <div className="row">
                    <div className="col-sm-6 col-sm-offset-2">
                        <div className="login-logo" />
                    </div>
                </div>
                <div className="row">
                    <div className="col-sm-6 col-sm-offset-2">
                        <div className="login-info">
                            {i18next.t('login.info')}
                        </div>
                    </div>
                </div>
                <div className="row">
                    <div className="col-xs-12">
                        <button onClick={() => this.buttonClick()}>INVIA</button>
                    </div>
                </div>
            </div>
        );
    }
}

export default LoginComponent;
