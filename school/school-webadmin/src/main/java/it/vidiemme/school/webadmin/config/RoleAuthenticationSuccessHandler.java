package it.vidiemme.school.webadmin.config;

import java.io.IOException;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.Authentication;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import it.vidiemme.school.business.security.UserPrincipal;
import it.vidiemme.school.business.service.UserService;
import it.vidiemme.school.webadmin.web.controller.ControllerHelper;

/**
 * Manages the post-login redirect/forward strategy, basing on the logged user role and its privileges.
 */
public class RoleAuthenticationSuccessHandler implements AuthenticationSuccessHandler {
    private static final Logger LOG = LoggerFactory.getLogger(RoleAuthenticationSuccessHandler.class);

    @Autowired
    UserService userService;    
    
    @Autowired
    ControllerHelper controllerHelper;
    
    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();

    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException {

        handle(request, response, authentication);
        clearAuthenticationAttributes(request);
    }

    protected void handle(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException {

        UserPrincipal userPrincipal = (UserPrincipal) authentication.getPrincipal();
        
        // after each login, the user last access date is saved on the database
        userService.updateLastAccess(userPrincipal.getUser());        
        
        String targetUrl = controllerHelper.getUserDefaultUrl(authentication);

        if (response.isCommitted()) {
            LOG.debug("Response has already been committed. Unable to redirect to " + targetUrl);
            return;
        }

        redirectStrategy.sendRedirect(request, response, targetUrl);
    }

    protected void clearAuthenticationAttributes(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session == null) {
            return;
        }
        session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
    }
}
