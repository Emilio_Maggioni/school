import React from 'react';
import { render } from 'react-dom';
// import { createStore } from 'redux';
// import { Provider } from 'react-redux';

import ErrorCatcher from './utilities/ErrorCatcher';
import Welcome from './components/Welcome';

import './index.scss';

const catchError = component => <ErrorCatcher>{component}</ErrorCatcher>;

$(() => {
    i18next.on('initialized', () => {
        const elements = Array.from(document.getElementsByClassName('react-component'));

        elements.map((element) => {
            let reactComponent = null;
            switch (element.getAttribute('data-type')) {
            case 'Welcome':
                reactComponent = catchError(<Welcome />);
                break;
            default:
                return new Error(`Unresolved element! ${element.getAttribute('data-type')}`);
            }
            if (reactComponent) {
                return render(reactComponent, element);
            }
            return null;
        });
    });
});

// registerServiceWorker();
