const login = {
    username: 'Username',
    password: 'Password',
    login: 'Accedi',
    info: 'Accedi al tuo account',
    forgot_password: 'Password dimenticata?',
};

export default login;
