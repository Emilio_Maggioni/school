package it.vidiemme.school.webadmin.web.controller;

import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.POST;

import java.io.IOException;
import java.util.List;

import javax.mail.MessagingException;
import javax.mail.internet.AddressException;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.mail.MailException;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import it.vidiemme.school.business.dto.DataTableRequestDTO;
import it.vidiemme.school.business.dto.DataTableResponseDTO;
import it.vidiemme.school.business.filter.UserFilter;
import it.vidiemme.school.business.model.User;
import it.vidiemme.school.business.service.UserService;
import it.vidiemme.school.business.validator.ValidationGroups.OnCreate;
import it.vidiemme.school.business.validator.ValidationGroups.OnUpdate;
import it.vidiemme.school.business.validator.constraint.ExistingUserId;
import it.vidiemme.school.webadmin.service.UsersEmailService;
import it.vidiemme.school.webadmin.web.form.ChangePasswordForm;
import it.vidiemme.school.webadmin.web.form.ForgotPasswordForm;
import it.vidiemme.school.webadmin.web.form.NewPasswordForm;
import it.vidiemme.school.webadmin.web.form.UserForm;
import it.vidiemme.school.webadmin.web.validator.ChangePasswordValidator;
import it.vidiemme.school.webadmin.web.validator.NewPasswordValidator;
import it.vidiemme.school.webadmin.web.validator.UserValidator;

@Controller
@Validated
@RequestMapping("/user")
public class UsersController {

    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private UserService userService;
    @Autowired
    private UserValidator userValidator;
    @Autowired
    private ChangePasswordValidator changePasswordValidator;
    @Autowired
    private NewPasswordValidator newPasswordValidator;
    @Autowired
    private UsersEmailService usersEmailService;
    @Autowired
    private MessageSource message;

    @PreAuthorize("hasAuthority('USERS_MANAGE')")
    @RequestMapping(method = {GET, POST})
    public String listUser(Model model) {
        loadUserForm(null, model);
        return "user/list";
    }
    
    @RequestMapping(value = "/pagination", consumes = MediaType.APPLICATION_JSON_UTF8_VALUE)
    public ResponseEntity<?> listUserPaginated(@RequestBody DataTableRequestDTO<UserFilter> dataTableRequestDTO, HttpServletRequest request){
        DataTableResponseDTO<List<User>> datatable = userService.getAllFilteredAndPaginated(dataTableRequestDTO);
        return ResponseEntity.status(HttpStatus.OK).body(datatable);         
    }

    @PreAuthorize("hasAuthority('USERS_MANAGE')")
    @GetMapping("/add")
    public String addUser(@ModelAttribute("userForm") UserForm userForm, BindingResult result, Model model) {
        loadUserForm(null, model);
        return "user/add";
    }

    @PreAuthorize("hasAuthority('USERS_MANAGE')")
    @PostMapping("/add")
    public String createUser(@ModelAttribute("userForm") @Validated(OnCreate.class) UserForm userForm, Errors errors, Model model, final RedirectAttributes redirectAttributes, HttpServletRequest req) throws MailException, AddressException, MessagingException, IOException {
        if (errors.hasErrors()) {
            loadUserForm(null, model);
            return "user/add";
        }
        User user = userService.addUser(userForm.toUser(), userForm.getRoles());
        usersEmailService.sendUserActivationMail(user, req);
        log.info(message.getMessage("user.created.successfully", null, null) );
        redirectAttributes.addFlashAttribute("msgOk", message.getMessage("user.created.successfully", null, null));
        return "redirect:/user";
    }
    
    @PreAuthorize("hasAuthority('USERS_MANAGE')")
    @GetMapping("/{id}/edit")
    public String editUser(@PathVariable("id") Integer id, Model model) {
       loadUserForm(id, model);
       return "user/edit";
    }

    @PreAuthorize("hasAuthority('USERS_MANAGE')")
    @PostMapping("/{id}/edit")
    public String updateUser(@ModelAttribute("userForm") @Validated(OnUpdate.class) UserForm userForm, Errors errors, Model model, RedirectAttributes redirectAttributes) {
            
        if (errors.hasErrors()) {
            loadUserForm(null, model);
            return "user/edit";
        }
        userService.update(userForm.toUser(), userForm.getRoles());
        redirectAttributes.addFlashAttribute("msgOK", message.getMessage("user.updated.successfully", null, null));
        return "redirect:/user";
    }

    @PreAuthorize("hasAuthority('USERS_MANAGE')")
    @DeleteMapping("/{id}/delete")
    public ResponseEntity<?> deleteUser(@ExistingUserId @PathVariable("id") Integer id, HttpServletRequest request, HttpServletResponse response) {
        userService.delete(id);
        return ResponseEntity.status(HttpStatus.OK).body(null);
    }

    @PreAuthorize("hasAuthority('USERS_MANAGE')")
    @GetMapping("/{id}/view")
    public String showUser(@PathVariable("id") Integer id, Model model) {
        loadUserForm(id, model);
        return "user/show";
    }
    
    @GetMapping("/set-new-password")
    public String setPasswordForm(@RequestParam("token") String token, @ModelAttribute("newPasswordForm") NewPasswordForm newPwdForm) {
        return "user/security/set_password";
    }
    
    @PostMapping("/set-new-password")
    public String setPassword(@ModelAttribute("newPasswordForm") @Validated NewPasswordForm newPasswordForm, Errors errors, Model model, HttpServletRequest request, final RedirectAttributes redirectAttributes) {
        if (errors.hasErrors()) {
            return "user/security/set_password";
        }
        User user = userService.setNewPassword(newPasswordForm.getToken(), newPasswordForm.getConfirmNewPassword());
        try {
            request.login(user.getUsername(), newPasswordForm.getNewPassword());
        }catch(ServletException ex){
            log.error("Error while login", ex);
        }
        return "redirect:/";
    }
    
    @GetMapping("/reset-password")
    public String resetPasswordForm(@ModelAttribute("forgotPwdForm") ForgotPasswordForm forgotPwdForm) {
        return "user/security/forgot_password";
    }

    @PostMapping("/reset-password")
    public String resetPassword(@ModelAttribute("forgotPwdForm") ForgotPasswordForm forgotPwdForm, Errors errors, Model model, final RedirectAttributes redirectAttributes, HttpServletRequest req) {
        usersEmailService.sendResetPasswordMail(forgotPwdForm.getEmail(), req);        
        redirectAttributes.addFlashAttribute("msgOk", message.getMessage("user.resetpassword.sent.ok", null, null));
        return "redirect:/user/reset-password";
    }
    
    @GetMapping("/change-password")
    public String changePasswordForm(@ModelAttribute("changePasswordForm") ChangePasswordForm changePasswordForm, Model model) {
        return "user/security/change_password";
    }

    @PostMapping("/change-password")
    public String changePassword(@ModelAttribute("changePasswordForm") @Validated ChangePasswordForm changePasswordForm, Errors errors, Model model, HttpServletRequest req, RedirectAttributes redirectAttributes) {
        if (errors.hasErrors()) {
            return "user/security/change_password";
        }
        userService.changePassword(changePasswordForm.getNewPassword());
        req.getSession().setAttribute("msgOk", message.getMessage("changePassword.password.updated", null, null));
        return "redirect:/login";
    }
    
    private void loadUserForm(Integer userId, Model model){
        if(userId != null){
            User user = userService.getById(userId, true);
            model.addAttribute("userForm", UserForm.fromUser(user));
        }
        model.addAttribute("roleList", userService.getAllRoles());
    }
    
    @InitBinder("userForm")
    public void setupUserBinder(WebDataBinder binder) {
        binder.addValidators(userValidator);
    }

    @InitBinder("newPasswordForm")
    public void setupNewPwdBinder(WebDataBinder binder) {
        binder.addValidators(newPasswordValidator);
    }
    
    @InitBinder("changePasswordForm")
    public void setupChangePwdBinder(WebDataBinder binder) {
        binder.addValidators(changePasswordValidator);
    }
}
