<%@ include file="/includes/taglibs.jsp"%>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>403 <spring:message code="project.brand"/></title>
	</head>
	<body>
		<div class="middle-box text-center animated fadeInDown">
			<h1>403</h1>
			<h3 class="font-bold"><spring:message code="forbidden.title"/></h3>
			<div class="error-desc"><spring:message code="forbidden.text"/></div>
		</div>
	</body>
</html>
