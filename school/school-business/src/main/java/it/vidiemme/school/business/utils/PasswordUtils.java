package it.vidiemme.school.business.utils;

import java.math.BigInteger;
import java.security.SecureRandom;

public class PasswordUtils {

    public static String generateRandomPassword() {
        SecureRandom random = new SecureRandom();
        BigInteger randomNumber = new BigInteger(130, random);
        return randomNumber.toString(32).substring(0, 8);
    }

}
