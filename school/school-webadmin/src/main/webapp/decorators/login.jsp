<%@ include file="/includes/taglibs.jsp"%>
<!DOCTYPE html>
<html lang="en">
<head>
    <sitemesh:write property='head'/>
    <title><sitemesh:write property='title'/></title>
</head>
<body>
    <div class="middle-box text-center loginscreen animated fadeInDown">
        <div>
            <div class="mb-3">
                <img src="${ctx}/assets/static/logo.png" alt="logo" />
            </div>
            <%@include file="/WEB-INF/jsp/success_message.jsp"%>
            <sitemesh:write property='body'/>
        </div>
    </div>
</body>
</html>
