/**
 * Classe che wrappa la classe Error per poter gestire anche dati custom
 *
 * @param {*} data Custom data
 * @param {string} reason Error description
 * @param {string} file
 * @param {number} line
 */
class CustomErrorWithData extends Error {
    constructor(data, reason, file, line) {
        super(reason, file, line);
        this.data = data;
    }
}

export default CustomErrorWithData;
