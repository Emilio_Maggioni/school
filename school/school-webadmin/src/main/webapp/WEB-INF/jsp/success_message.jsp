<%@include file="/includes/taglibs.jsp"%>
<c:if test="${not empty msgOk}" var="testSV_OK">
	<div class="row">
	    <div class="col-12">
		    <div class="alert alert-success alert-dismissable">
		    	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">
                    <i class="fa fa-times"></i>
			   	</button>
		        ${msgOk}
		    </div>
	    </div>
    </div>
</c:if>
