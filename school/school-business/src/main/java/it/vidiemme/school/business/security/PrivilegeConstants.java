package it.vidiemme.school.business.security;

public interface PrivilegeConstants {
    public final static String EXAMPLE_SECTION_VIEW = "EXAMPLE_SECTION_VIEW";
    public final static String USERS_MANAGE = "USERS_MANAGE";
}
