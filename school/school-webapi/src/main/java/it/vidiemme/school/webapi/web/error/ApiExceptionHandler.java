package it.vidiemme.school.webapi.web.error;

import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

import javax.validation.ConstraintViolationException;

import org.springframework.beans.ConversionNotSupportedException;
import org.springframework.beans.TypeMismatchException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.http.converter.HttpMessageNotWritableException;
import org.springframework.validation.BindException;
import org.springframework.web.HttpMediaTypeNotAcceptableException;
import org.springframework.web.HttpMediaTypeNotSupportedException;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingPathVariableException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.ServletRequestBindingException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.context.request.async.AsyncRequestTimeoutException;
import org.springframework.web.multipart.support.MissingServletRequestPartException;

import com.google.common.base.CaseFormat;

import it.vidiemme.school.business.exception.ServiceException;

@ControllerAdvice
public class ApiExceptionHandler {

    @ExceptionHandler(MethodArgumentNotValidException.class)
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, WebRequest request) {

        List<ApiFieldError> apiFieldErrors = ex.getBindingResult().getFieldErrors().stream()
                .map(fieldError -> new ApiFieldError(toSnake(fieldError.getField()), fieldError.getCode(), fieldError.getRejectedValue(), fieldError.getArguments())).collect(Collectors.toList());

        List<ApiGlobalError> apiGlobalErrors = ex.getBindingResult().getGlobalErrors().stream().map(globalError -> new ApiGlobalError(globalError.getCode(), globalError.getArguments()))
                .collect(Collectors.toList());

        ApiErrorResponse apiErrorResponse = new ApiErrorResponse(apiFieldErrors, apiGlobalErrors);

        return new ResponseEntity<>(apiErrorResponse, HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @ExceptionHandler(ConstraintViolationException.class)
    protected ResponseEntity<Object> handleConstraintViolationException(ConstraintViolationException ex) {

        List<ApiFieldError> apiFieldErrors = ex.getConstraintViolations().stream()
                .map(violation -> new ApiFieldError(violation.getPropertyPath().toString(), violation.getMessage(), violation.getInvalidValue().toString(), null)).collect(Collectors.toList());

        ApiErrorResponse apiErrorResponse = new ApiErrorResponse(apiFieldErrors, Collections.emptyList());

        return new ResponseEntity<>(apiErrorResponse, HttpStatus.UNPROCESSABLE_ENTITY);
    }

    @ExceptionHandler({ HttpRequestMethodNotSupportedException.class, HttpMediaTypeNotSupportedException.class, HttpMediaTypeNotAcceptableException.class, MissingPathVariableException.class,
            MissingServletRequestParameterException.class, ServletRequestBindingException.class, ConversionNotSupportedException.class, TypeMismatchException.class,
            HttpMessageNotReadableException.class, HttpMessageNotWritableException.class, MissingServletRequestPartException.class, BindException.class, AsyncRequestTimeoutException.class })
    protected ResponseEntity<Object> handleControllerException(Exception ex, HttpHeaders headers, HttpStatus status) {

        List<ApiGlobalError> apiGlobalErrors = Collections.singletonList(new ApiGlobalError(ex.getMessage(), null));
        ApiErrorResponse apiErrorResponse = new ApiErrorResponse(null, apiGlobalErrors);
        return new ResponseEntity<>(apiErrorResponse, HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler({ ServiceException.class })
    protected ResponseEntity<Object> handleServiceException(Exception ex) {

        List<ApiGlobalError> apiGlobalErrors = Collections.singletonList(new ApiGlobalError(ex.getMessage(), null));
        ApiErrorResponse apiErrorResponse = new ApiErrorResponse(null, apiGlobalErrors);
        return new ResponseEntity<>(apiErrorResponse, HttpStatus.BAD_REQUEST);
    }

    private String toSnake(String s) {
        return CaseFormat.LOWER_CAMEL.to(CaseFormat.LOWER_UNDERSCORE, s);
    }

    //@ExceptionHandler(NoHandlerFoundException.class)
}
